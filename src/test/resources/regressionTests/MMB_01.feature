Feature: Verify features of MMB Dropdown list 

@MMB
Scenario: Verify dropdown list for select client on MMB Portal
Given user launched the browser
Then user enter username "arjun-bhatia" and password "Mercer@12345678"
Then user is on the mmb home page
And select client tab should be displayed
Then user selects select client tab
Then Verify that search navigation bar link is displayed
|Choose Another Client|
Then user clicks on choose a client dropdown
And user logout from the application
