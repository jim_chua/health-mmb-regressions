Feature: Verify user is able to create survey 

@EE
Scenario: Verify create survey from PA link
Given user launched the browser
When user enter username "DeepakAdmin" and password "Survey@2019"
When user clicks on Admin
And user mouse hover on content management
And user clicks on Survey Setup
When user clicks on Create Survey
And user create the survey
And user clicks on save button
Then survey created successfully message should displayed
When user clicks on Create SubGroup
And user enter discription "Test subgroup"
And enter subggroup value as "1"
And user clicks on SubGroup save button
And user clicks on save button
And user click on the back button
When user clicks on the publish link
And user enter start date 
And user enter end date
And user enter broadcasting date
And user clicks on Publish button
And user logout from the application

