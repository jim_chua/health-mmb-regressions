Feature: Verify features of All Claims Page

@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user select all claims link
When user click on the paid link
Then verify paid claim table is displayed on the page
|06-Jan-2015|
And user logout from the application


@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user select all claims link
When user click on the rejected link
Then verify rejected claim table is displayed on the page
|There are no results available.|
And user logout from the application



@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user select all claims link
When user click on the pending link
Then verify pending claim table is displayed on the page
|04-Jan-2019|
And user logout from the application