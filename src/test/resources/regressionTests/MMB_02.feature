Feature: Verify features of MMB Dropdown selection  from list 

@MMB
Scenario: Verify user is able to select client from dropdown list for select client on MMB Portal
Given user launched the browser
Then user enter username "arjun-bhatia" and password "Mercer@12345678"
Then user is on the mmb home page
Then user selects select client tab
Then user has selected BenefitMe Indonesia PT from the dropdown
And user clicks on select button
Then verify BenefitMe Indonesia PT page should display
And user logout from the application
