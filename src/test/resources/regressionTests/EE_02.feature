Feature: Verify home page features of Employee Portal Home Page

@EE
Scenario Outline: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
Then verify selected <Name> item name are displayed in Check out page
And user logout from the application

Examples:
|Name				|
|Your Claims		|
