Feature: Verify features of All Claims Page

@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user select all claims link
Then verify that all claims link should display on the page
|All Claims|View your claims below.|
And user logout from the application


