Feature: Verify features of MMB Client Password view setting page 

@MMB
Scenario: Verify client set up of admin page on MMB Portal
Given user launched the browser
Then user enter username "arjun-bhatia" and password "Mercer@12345678"
And user is on the mmb home page
When user selects admin dropdown link
Then user select client setup from dropdown list
Then verify that "Client HR" is displayed in the dropdown
Then verify that "Client Employee" is displayed in the dropdown
Then verify that "Pending Employees" is displayed in the dropdown
Then verify that export link is displayed on the page
And user logout from the application