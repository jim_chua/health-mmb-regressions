Feature: Verify features of MMB Portal Setting page 

@MMB
Scenario: Verify various fields and link present on Portal Setting page on MMB Portal
Given user launched the browser
Then user enter username "arjun-bhatia" and password "Mercer@12345678"
Then user is on the mmb home page
When user selects admin dropdown link
Then user select portal setup from dropdown list
Then portal setup page should be displayed
Then setting dropdown should be displayed
Then user clicks on Setting dropdown link
Then verify admin portal settings name should be displayed in the dropdown list
|Help Section|Client Legal Info|
And user logout from the application

