Feature: User should be able to search and view documents in employee portal

@EE
Scenario Outline: Verify that user can search for an existing document
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
Then verify that documents panel is displayed 
When user clicks on view all documents link
Then verify that documents page is displayed
Then user searches for a document with name <document>
Then verify that at least one document with name <document> is returned
And user logout from the application

Examples:
|document|
|BRD|