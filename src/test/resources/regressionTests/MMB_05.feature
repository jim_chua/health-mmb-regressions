Feature: Verify features of MMB Client Password view setting page 

@MMB
Scenario: Verify password Policy of view Setting page on MMB Portal
Given user launched the browser
Then user enter username "arjun-bhatia" and password "Mercer@12345678"
And user is on the mmb home page
When user selects admin dropdown link
Then user select portal setup from dropdown list
Then portal setup page should be displayed
Then verify view link should be displayed under portal setting policy text
Then user selects view link under portal setting policy
Then verify portal setting policy view page should be displayed
And user logout from the application