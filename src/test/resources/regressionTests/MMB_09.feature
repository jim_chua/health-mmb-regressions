Feature: Verify reports page on MMB Portal

@MMB
Scenario: Verify reports page on MMB Portal
Given user launched the browser
Then user enter username "arjun-bhatia" and password "Mercer@12345678"
And user is on the mmb home page
When user click on the invoice details link
Then verify that invoice details page should displayed
And user logout from the application