Feature: Verify user is able to create survey 

@EE
Scenario: Verify user is able to create Single selection question in EEAdmin survey on PA page
Given user launched the browser
When user enter username "DeepakAdmin" and password "Survey@2019"
And user clicks on Admin
And user mouse hover on content management
And user clicks on Survey Setup
When user clicks on Create Survey
And user create the survey
And user clicks on save button
Then survey created successfully message should displayed
When user clicks on Create SubGroup
And user enter discription "Test subgroup"
And enter subggroup value as "1"
And user clicks on SubGroup save button
And user clicks on save button
And user click on create Question
Then Survey Setup (Question) page should display
When User enter "This is the single selection question" in textbox
And user selects type as single selection from drop down
And  user enter option in Display Order as "1"
And  user enter option in New Option as "option1"
And  user selects option Is Correct Answer
And  user clicks on Add Options
And  user enter option in New Option as "option2"
And  user clicks on Add Options
And  user enter option in New Option as "option3"
And  user clicks on Add Options
And User clicks on save question button
Then Question created successfully message should be displayed





