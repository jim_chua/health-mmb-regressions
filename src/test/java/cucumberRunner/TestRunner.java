package cucumberRunner;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.vimalselvam.cucumber.listener.ExtentProperties;
import com.vimalselvam.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\resources\\regressionTests", glue = "stepDefinations", tags = "@MMB", 
strict = true, monochrome = true, dryRun = false, plugin = {
		"pretty", "html:target/cucumber", "json:target/cucumber.json", "junit:target/cukes.xml",
		"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:" })

public class TestRunner {

	@BeforeClass
	public static void setup() {
		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		extentProperties.setReportPath("target/extent-report/MMB_Automation_Report.html");
	}

	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
		Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
		Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		Reporter.setSystemInfo("Machine", "Windows 10" + "64 Bit");
		Reporter.setSystemInfo("Selenium", "3.4.0");
		Reporter.setSystemInfo("Maven", "3.6.2");
		Reporter.setSystemInfo("Java Version", "1.8");
	}

}
