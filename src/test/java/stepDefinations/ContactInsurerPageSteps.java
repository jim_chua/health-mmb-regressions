package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumberUtilities.TestContext;
import pageObjects.ContactInsurerPage;

public class ContactInsurerPageSteps  {
	
	
	ContactInsurerPage contactInsurerPage;
	TestContext testContext;
	
	public ContactInsurerPageSteps(TestContext context) {
		testContext = context;
		contactInsurerPage = testContext.getPageObjectManager().getContactInsurerPage();
	}
	
	
	@Then("^verify contact insurer page is displayed$")
	public void verify_contact_insurer_page_is_displayed(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(contactInsurerPage.getInsurerName().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(contactInsurerPage.getSubjectMatterExpert().getText().contains(object.get(0).get(1)));
	}

}
