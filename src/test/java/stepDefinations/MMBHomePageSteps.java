package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.MMBHomePage;

public class MMBHomePageSteps extends Base {

	MMBHomePage mmbHomePage;
	TestContext testContext;

	public MMBHomePageSteps(TestContext context) {
		testContext = context;
		mmbHomePage = testContext.getPageObjectManager().getMMBHomePage();
	}
	
	@When("^user click on the others dropdown$")
	public void user_click_on_the_others_dropdown() throws Throwable {
	   mmbHomePage.clickOthersDropdown();
	}

	@When("^user click on the news link$")
	public void user_click_on_the_news_link() throws Throwable {
	  mmbHomePage.clickNewsLink();
	}

	@Then("^verify that published for hr lony text should be displyed on the page$")
	public void verify_that_published_for_hr_lony_text_should_be_displyed_on_the_page() throws Throwable {
		waitFortheElement(30, mmbHomePage.getPublishedForHROnlyText());
	   mmbHomePage.getPublishedForHROnlyText().getText().contains("Published For HR Only");
	}
	
	@Then("^user select client setup from dropdown list$")
	public void user_select_client_setup_from_dropdown_list() throws Throwable {
	    mmbHomePage.clickClientUsersLink();
	}

	@Then("^Verify that search navigation bar link is displayed$")
	public void verify_that_search_navigation_bar_link_is_displayed(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		waitFortheElement(60, mmbHomePage.getChooseAnotherClientText());
		Thread.sleep(2000);
		Assert.assertTrue(mmbHomePage.getChooseAnotherClientText().getText().contains(object.get(0).get(0)));
		//Assert.assertTrue(mmbHomePage.getMMBChooseAClientSelectButton().getText().contains(object.get(0).get(1)));
		
	}
	
	@Then("^user is on the mmb home page$")
	public void user_is_on_the_mmb_home_page() throws Throwable {
		mmbHomePage.getMMBHomePageHeader();
	}

	@When("^user selects admin dropdown link$")
	public void user_selects_admin_dropdown_link() throws Throwable {
		mmbHomePage.clickAdminTab();
	}


	@Then("^user select portal setup from dropdown list$")
	public void user_select_portal_setup_from_dropdown_list() throws Throwable {
		mmbHomePage.clickPortalSetupLink();
	}
	
	@And("^select client tab should be displayed$")
	public void select_client_tab_should_be_displayed() throws Throwable {
	   mmbHomePage.getMMBClientLink();
	}

	@Then("^user selects select client tab$")
	public void user_selects_select_client_tab() throws Throwable {
	   mmbHomePage.clickMMBCLientLink();
	}
	
	@Then("^choose another client feild should display$")
	public void choose_another_client_feild_should_display() throws Throwable {
	   mmbHomePage.getChooseAnotherClientText();
	}
	
	@And("^choose a client dropdown should display$")
	public void choose_a_client_dropdown_should_display() throws Throwable {
	   mmbHomePage.getMMBChooseAClient();
	}
	
	@And("^select button should display$")
	public void select_button_should_display() throws Throwable {
	   mmbHomePage.getMMBChooseAClientSelectButton();
	}
	
	@Then("^user clicks on choose a client dropdown$")
	public void user_clicks_on_choose_a_client_dropdown() throws Throwable {
	   mmbHomePage.clickMMBChooseAClient();
	}
	
	@Then("^verify benefitMe Indonesia PT should be displayed in dropdown$")
	public void verify_benefitMe_Indonesia_PT_should_be_displayed_in_dropdown() throws Throwable {
	   mmbHomePage.getMMBChooseAClientIndonesia().getText().contains("BenefitMe Indonesia, PT");
	}
	
	@Then("^user has selected BenefitMe Indonesia PT from the dropdown$")
	public void user_has_selected_BenefitMe_Indonesia_PT_from_the_dropdown() throws Throwable {
		mmbHomePage.clickMMBChooseAClientIndonesia();
	}
	
	
	@And("^user clicks on select button$")
	public void user_clicks_on_select_button() throws Throwable {
	   mmbHomePage.clickMMBChooseAClientSelectButton();
	}
	
	@Then("^verify BenefitMe Indonesia PT page should display$")
	public void verify_BenefitMe_Indonesia_PT_page_should_display() throws Throwable {
	   mmbHomePage.getMMBClientNameHeader().getText().contains("BenefitMe Indonesia, PT");
	}
	
	@When("^user click on the audit trail for the enrolment link$")
	public void user_click_on_the_audit_trail_for_the_enrolment_link() throws Throwable {
	   mmbHomePage.clickauditTrailForEnrolment();
	}

	
	
	@When("^user click on the invoice details link$")
	public void user_click_on_the_invoice_details_link() throws Throwable {
	    mmbHomePage.clickInvoiceDetails();
	}

}
