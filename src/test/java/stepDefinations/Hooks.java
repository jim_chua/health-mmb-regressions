package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.google.common.io.Files;
import com.vimalselvam.cucumber.listener.Reporter;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumberUtilities.TestContext;
import cucumbersManagers.FileReaderManager;
import cucumbersManagers.PageObjectManager;
import cucumbersManagers.WebDriverManager;

public class Hooks {

	TestContext testContext;

	public Hooks(TestContext context) {
		testContext = context;
	}

	@Before("@MMB")
	public void beforeScenario(Scenario scenario) {
		testContext.webDriverManager = new WebDriverManager();
		testContext.pageObjectManager = new PageObjectManager(testContext.webDriverManager.getDriver(scenario));
		System.out.println("MMB Application Automation");
		System.out.println("Scenario name: " + scenario.getName());

	}

	@After
	public void afterScenario() throws InterruptedException {
		testContext.getWebDriverManager().closeDriver();
	}

	@After("@MMB")
	public void afterScenario(Scenario scenario) {
		if (scenario.isFailed()) {
			String screenshotName = scenario.getName().replaceAll(" ", "_");
			try {
				// This takes a screenshot from the driver at save it to the specified location
				File sourcePath = ((TakesScreenshot) testContext.getWebDriverManager().getDriver(scenario))
						.getScreenshotAs(OutputType.FILE);

				// Building up the destination path for the screenshot to save
				// Also make sure to create a folder 'screenshots' in the project level
				// folder
				File destinationPath = new File(
						System.getProperty("user.dir") + "\\screenShot\\" + screenshotName + ".png");

				// Copy taken screenshot from source location to destination location
				Files.copy(sourcePath, destinationPath);

				// This attach the specified screenshot to the test
				Reporter.addScreenCaptureFromPath(destinationPath.toString());
			} catch (IOException e) {
			}
		}
	}

}
