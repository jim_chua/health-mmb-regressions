package stepDefinations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.LoginPage;

public class LoginPageSteps extends Base {

	LoginPage loginPage;
	TestContext testContext;

	public LoginPageSteps(TestContext context) {
		testContext = context;
		loginPage = testContext.getPageObjectManager().getLoginPage();
	}

	
	@Then("^user logout from the application$")
	public void user_logout_from_the_application() throws Throwable {
	    loginPage.clickLogoutArrow();
	    loginPage.clickLogout();
	}
	
	@Given("^user launched the browser$")
	public void user_launched_the_browser() throws Throwable {
		System.out.println("User launch the browser");
	}
	
	@Then("^user enter username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void user_enter_username_and_password(String Username, String Password) throws Throwable {
		waitFortheElement(60, loginPage.signInButton);
		loginPage.signIn();
		loginPage.getUsernamePassword(Username, Password);
		loginPage.login();
	

	}
	
	@Then("^user click on the login as employee button$")
	public void user_click_on_the_login_as_employee_button() throws Throwable {
		loginPage.loginAsEmployee();
	}
	
	@Then("^user click on the login as HR button$")
	public void user_click_on_the_login_as_HR_button() throws Throwable {
	    loginPage.loginAsHR();
	}

}
