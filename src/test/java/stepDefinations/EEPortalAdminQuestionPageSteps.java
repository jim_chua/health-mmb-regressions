package stepDefinations;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.EEPortalAdminQuestionPage;

public class EEPortalAdminQuestionPageSteps extends Base{
	
	EEPortalAdminQuestionPage eePortalAdminQuestionPage;
	TestContext testContext;
	
	public EEPortalAdminQuestionPageSteps(TestContext context) {
		testContext = context;
		eePortalAdminQuestionPage = testContext.getPageObjectManager().getEEPortalAdminQuestionPage();
	}
	
	
	@When("^user click on create Question$")
	public void user_click_on_create_Question() throws Throwable {
		 eePortalAdminQuestionPage.clickCreateQuestionLink();
	}

	@Then("^Survey Setup \\(Question\\) page should display$")
	public void survey_Setup_Question_page_should_display() throws Throwable {
	   eePortalAdminQuestionPage.getServeySetupQuestionHeaderText().getText().contains("Survey Setup (Question)");
	}

	@When("^User enter \"([^\"]*)\" in textbox$")
	public void user_enter_in_textbox(String arg1) throws Throwable {
	    eePortalAdminQuestionPage.enterServeySetupQuestionBox(arg1);
	}


	@When("^user selects type as single selection from drop down$")
	public void user_selects_type_as_single_selection_from_drop_down() throws Throwable {
	    eePortalAdminQuestionPage.clickServeySetupTypeSingle();
	}

	@When("^user enter option in Display Order as \"([^\"]*)\"$")
	public void user_enter_option_in_Display_Order_as(String arg1) throws Throwable {
	  eePortalAdminQuestionPage.enterServeySetupDisplayOrder(arg1);
	}

	@When("^user enter option in New Option as \"([^\"]*)\"$")
	public void user_enter_option_in_New_Option_as(String arg1) throws Throwable {
	    eePortalAdminQuestionPage.enterServeySetupNewOption(arg1);
	}

	@When("^user selects option Is Correct Answer$")
	public void user_selects_option_Is_Correct_Answer() throws Throwable {
	    eePortalAdminQuestionPage.clickServeySetupCorrectCheckBox();
	}

	@When("^user clicks on Add Options$")
	public void user_clicks_on_Add_Options() throws Throwable {
	   eePortalAdminQuestionPage.clickserveySetupAddOption();
	}

	@When("^User clicks on save question button$")
	public void user_clicks_on_save_question_button() throws Throwable {
	    eePortalAdminQuestionPage.clickSaveButton();
	}

	@Then("^Question created successfully message should be displayed$")
	public void question_created_successfully_message_should_be_displayed() throws Throwable {
	   eePortalAdminQuestionPage.getserveySetupQuestionCreatedMessage().getText().contains("Question created successfully.");
	}

}
