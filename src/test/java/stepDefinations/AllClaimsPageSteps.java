package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import pageObjects.AllClaimsPage;

public class AllClaimsPageSteps {
	
	AllClaimsPage allClaimsPage;
	TestContext testContext;
	
	public AllClaimsPageSteps(TestContext context) {
		testContext = context;
		allClaimsPage = testContext.getPageObjectManager().getAllClaimsPage();
	}
	
	@When("^user click on the paid link$")
	public void user_click_on_the_paid_link() throws Throwable {
	    allClaimsPage.clickPaidLink();
	}

	@Then("^verify paid claim table is displayed on the page$")
	public void verify_paid_claim_table_is_displayed_on_the_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(allClaimsPage.getPaidClaimTable().getText().contains(object.get(0).get(0)));
		
	}

	@Then("^verify rejected claim table is displayed on the page$")
	public void verify_rejected_claim_table_is_displayed_on_the_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(allClaimsPage.getRejectedClaimTable().getText().contains(object.get(0).get(0)));
	}

	@When("^user click on the pending link$")
	public void user_click_on_the_pending_link() throws Throwable {
	   allClaimsPage.clickpendingClaimLink();
	}

	@Then("^verify pending claim table is displayed on the page$")
	public void verify_pending_claim_table_is_displayed_on_the_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(allClaimsPage.getPendingClaimTable().getText().contains(object.get(0).get(0)));
	}
	
	@Then("^verify that all claims link should display on the page$")
	public void verify_that_all_claims_link_should_display_on_the_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(allClaimsPage.getAllLink().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(allClaimsPage.getviewYourClaim().getText().contains(object.get(0).get(1)));
	}
	
	@When("^user click on the rejected claim link$")
	public void user_click_on_the_rejected_claim_link() throws Throwable {
		allClaimsPage.clickrejectedClaimLink();
	}

	@Then("^verify that rejected claim page should display$")
	public void verify_that_rejected_claim_page_should_display(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(allClaimsPage.getRejectedClaimLinkText().getText().contains(object.get(0).get(0)));
	}

	@When("^user click on the pending claim link$")
	public void user_click_on_the_pending_claim_link() throws Throwable {
		allClaimsPage.clickpendingClaimLink();
	}

	@Then("^verify that pending claim page should display$")
	public void verify_that_pending_claim_page_should_display(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(allClaimsPage.getPendingClaimLinkText().getText().contains(object.get(0).get(0)));
	}

	@When("^user click on the download claim link$")
	public void user_click_on_the_download_claim_link() throws Throwable {
		allClaimsPage.clickdownloadClaimLink();
	}

	@Then("^verify that download claim page should display$")
	public void verify_that_download_claim_page_should_display(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(allClaimsPage.getDownloadClaimLinkText().getText().contains(object.get(0).get(0)));
	}
	
	
	
	

}
