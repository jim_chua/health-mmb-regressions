package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.HRClaimsPage;

public class HRClaimsPageSteps extends Base{

	HRClaimsPage hrClaimsPage;
	TestContext testContext;
	
	public HRClaimsPageSteps(TestContext context) {
		testContext = context;
		hrClaimsPage = testContext.getPageObjectManager().getHRClaimsPage();
	}
	
	@Then("^\"([^\"]*)\" text should should display$")
	public void text_should_should_display(String name) throws Throwable {
		waitFortheElement(60, hrClaimsPage.getClaimsText());
		Assert.assertTrue(hrClaimsPage.getClaimsText().getText().contains(name));
	}
	
	@Then("^user click on the Pending link$")
	public void user_click_on_the_Pending_link() throws Throwable {
	    hrClaimsPage.clickPendingLink();
	}

	@Then("^verify that claim number is displayed in the pending claim table$")
	public void verify_that_claim_number_is_displayed_in_the_pending_claim_table(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		waitFortheElement(60, hrClaimsPage.getPendingClaimNo1());
		Thread.sleep(2000);
		Assert.assertTrue(hrClaimsPage.getPendingClaimNo1().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(hrClaimsPage.getPendingClaimNo2().getText().contains(object.get(0).get(1)));
	}
	
	@Then("^user click on the Paid link$")
	public void user_click_on_the_Paid_link() throws Throwable {
	   hrClaimsPage.clickPaidLink();
	}

	@Then("^verify that paid claim detail page should display$")
	public void verify_that_paid_claim_detail_page_should_display() throws Throwable {
		Thread.sleep(2000);
	    System.out.println("There are no results that match your criteria. Please modify your search and try again");
	}
	
	@Then("^user click on the rejected link$")
	public void user_click_on_the_rejected_link() throws Throwable {
	   hrClaimsPage.clickRejectedLink();
	}

	@Then("^verify that rejected claim detail page should display$")
	public void verify_that_rejected_claim_detail_page_should_display() throws Throwable {
		Thread.sleep(2000);
		System.out.println("There are no results that match your criteria. Please modify your search and try again");
	}

	@When("^user select payment status as cancelled from the dropdown$")
	public void user_select_payment_status_as_cancelled_from_the_dropdown() throws Throwable {
	    hrClaimsPage.selectPendingStatusAsCancelled();
	}


	@Then("^verify that payment status cancelled table is displayed on the page$")
	public void verify_that_payment_status_cancelled_table_is_displayed_on_the_page() throws Throwable {
		Thread.sleep(2000);
		System.out.println("There are no results that match your criteria. Please modify your search and try again");
	}
	
}
