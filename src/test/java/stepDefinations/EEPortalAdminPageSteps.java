package stepDefinations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.EEPortalAdminPage;

public class EEPortalAdminPageSteps extends Base{
	
	//private static final String String = null;
	EEPortalAdminPage eePortalAdminPage;
	TestContext testContext;
	
	public EEPortalAdminPageSteps(TestContext context) {
		testContext = context;
		eePortalAdminPage = testContext.getPageObjectManager().getEEPortalAdminPage();
	}
	
	@When("^user clicks on Admin$")
	public void user_clicks_on_Admin() throws Throwable {
		eePortalAdminPage.clickAdminDropDown();
	}

	@And("^user mouse hover on content management$")
	public void user_mouse_hover_on_content_management() throws Throwable {
		eePortalAdminPage.clickContentMnagement();
	}

	@And("^user clicks on Survey Setup$")
	public void user_clicks_on_Survey_Setup() throws Throwable {
		eePortalAdminPage.clickSurveySetupLink();
	}

	@Then("^survey Setup page should display$")
	public void survey_Setup_page_should_display() throws Throwable {
		eePortalAdminPage.getSurveySetupPageText().getText().contains("Survey Setup");
	}

	@When("^user clicks on Create Survey$")
	public void user_clicks_on_Create_Survey() throws Throwable {
		eePortalAdminPage.clickCreateSurveyButton();
	}
	
	
	@And("^user create the survey$")
	public void user_create_the_survey() throws Throwable {
		eePortalAdminPage.enterSurveyName();
	}
	
	@And("^user click on the back button$")
	public void user_click_on_the_back_button() throws Throwable {
	  eePortalAdminPage.clickBackButton();
	}
	
	
	@And("^user clicks on save button$")
	public void user_clicks_on_save_button() throws Throwable {
		eePortalAdminPage.clickSaveSurveyButton();
	}
	
	@Then("^survey created successfully message should displayed$")
	public void survey_created_successfully_message_should_displayed() throws Throwable {
		eePortalAdminPage.getcreateSurveyMessage().getText().contains("Survey created successfully.");
	}
	
	@When("^user clicks on Create SubGroup$")
	public void user_clicks_on_Create_SubGroup() throws Throwable {
		eePortalAdminPage.clickCreateSubGroupButton();
	}
	
	
	
	@And("^user enter discription \"([^\"]*)\"$")
	public void user_enter_discription(String Description) throws Throwable {
	   eePortalAdminPage.enterDescription(Description);
	}

	@And("^enter subggroup value as \"([^\"]*)\"$")
	public void enter_subggroup_value_as(String DisplayOrder) throws Throwable {
	   eePortalAdminPage.enterDisplayOrder(DisplayOrder);
	}
	
	
	@And("^user clicks on SubGroup save button$")
	public void user_clicks_on_SubGroup_save_button() throws Throwable {
		eePortalAdminPage.clickSubGroupSaveButton();
	}
	
	@Then("^verify survey Subgroup added Sucessfully message should displayed$")
	public void verify_survey_Subgroup_added_Sucessfully_message_should_displayed() throws Throwable {
		eePortalAdminPage.getsubGroupSaveMessage().getText().contains("Survey Subgroup added Sucessfully.");
	}
	
	//New
	@And("^user enter \"([^\"]*)\" in survey name textbox$")
	public void user_enter_in_survey_name_textbox(String SName) throws Throwable {
		eePortalAdminPage.enterSearchSurveyName(SName);
		
	}

	@And("^user clicks on the search button$")
	public void user_clicks_on_the_search_button() throws Throwable {
		eePortalAdminPage.clickSearchButton();
	}

	@Then("^verify survey name should display$")
	public void verify_survey_name_should_display() throws Throwable {
		eePortalAdminPage.getSearchSurveyNameText().getText().contains("Arjun");
	}

	@When("^user clicks on the publish link$")
	public void user_clicks_on_the_publish_link() throws Throwable {
		eePortalAdminPage.clickPublishLink();
	}


	@And("^user clicks on Publish button$")
	public void user_clicks_on_Publish_button() throws Throwable {
		eePortalAdminPage.clickPublishButtonSurvey();
	}

	@Then("^verify Survey Published Successfully message should display$")
	public void verify_Survey_Published_Successfully_message_should_display() throws Throwable {
		eePortalAdminPage.getPublishMessage().getText().contains("Survey Published Successfully.");
	}
	
	@When("^user enter start date$")
	public void user_enter_start_date() throws Throwable {
	    eePortalAdminPage.enterStartDate();
	}

	@When("^user enter end date$")
	public void user_enter_end_date() throws Throwable {
	    eePortalAdminPage.enterEndDate();
	}

	@When("^user enter broadcasting date$")
	public void user_enter_broadcasting_date() throws Throwable {
	    eePortalAdminPage.enterBroadcastingDate();
	}



}