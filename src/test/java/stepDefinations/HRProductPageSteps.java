package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.HRProductPage;

public class HRProductPageSteps extends Base {
	
	
	HRProductPage hrProductPage;
	TestContext testContext;
	
	public HRProductPageSteps(TestContext context) {
		testContext = context;
		hrProductPage = testContext.getPageObjectManager().getHRProductPage();
	}



	@When("^user click on the search button$")
	public void user_click_on_the_search_button() throws Throwable {
	    hrProductPage.clickSearchButton();
	}

	@Then("^verify \"([^\"]*)\" link should display on the page$")
	public void verify_link_should_display_on_the_page(String link) throws Throwable {
		waitFortheElement(60, hrProductPage.getExportLink());
		Assert.assertTrue(hrProductPage.getExportLink().getText().contains(link));
	}
	
	@Then("^verify active and expired link should display on the page$")
	public void verify_active_and_expired_link_should_display_on_the_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		waitFortheElement(60, hrProductPage.getActiveLink());
		Assert.assertTrue(hrProductPage.getActiveLink().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(hrProductPage.getExpiredLink().getText().contains(object.get(0).get(1)));
	}
	
	
	@When("^user click on the expired link$")
	public void user_click_on_the_expired_link() throws Throwable {
	   hrProductPage.clickExpiredLink();
	}

	@Then("^verify that expired table is displayed$")
	public void verify_that_expired_table_is_displayed() throws Throwable {
	   System.out.println("Expired table is not available on the page");
	}
	
}
