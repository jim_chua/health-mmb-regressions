package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.SurveyPage;

public class SurveyPageSteps extends Base {

	SurveyPage surveyPage;
	TestContext testContext;

	public SurveyPageSteps(TestContext context) {
		testContext = context;
		surveyPage = testContext.getPageObjectManager().getSurveyPage();
	}

	@Then("^verify that survey page is displayed$")
	public void verify_survey_list_page() throws Throwable {
		waitFortheElement(60, surveyPage.getSurveyHeaderName());
		waitFortheElement(60, surveyPage.getLatestSurveyName());
	}
	
	@Then("^user clicks on the first survey in the list$")
	public void user_click_on_the_first_survey() throws Throwable {
		waitFortheElement(60, surveyPage.getLatestSurveyName());
		surveyPage.clickFirstSurvey();
	}
	
	@Then("^verify that terms and agreement page is displayed$")
	public void verify_terms_agreement_page() throws Throwable {
		waitFortheElement(60, surveyPage.getTermsAgreementText());
		Assert.assertTrue(surveyPage.getTermsAgreementText().getText().contains("Terms"));
	}
	
	@Then("^user clicks on the agree button$")
	public void user_click_agree_survey() throws Throwable {
		surveyPage.clickAgreeTermsButton();
	}
	
	@Then("^verify that survey form is displayed$")
	public void verify_survey_form_is_displayed() throws Throwable {
		waitFortheElement(60, surveyPage.getSurveyFormName());
	}
	
	@Then("^user selects options on the survey$")
	public void user_select_survey_options_on_survey(DataTable data) throws Throwable {
			
		List<List<String>> object = data.raw();
		
		Assert.assertTrue(surveyPage.get_Q1option2().getText().contains(object.get(0).get(0)));
		clickButton(surveyPage.get_Q1option2());
		
		Assert.assertTrue(surveyPage.get_Q2option1().getText().contains(object.get(0).get(1)));
		clickButton(surveyPage.get_Q2option1());
		
		Assert.assertTrue(surveyPage.get_Q2option3().getText().contains(object.get(0).get(2)));
		clickButton(surveyPage.get_Q2option3());	
	}

	@Then("^user enters \"([^\"]*)\" on the input field$")
	public void user_enters_answer_on_survey(String answer) throws Throwable {
		
		surveyPage.enterSurveyFreeTextAnswer(answer);
	}
	
	@Then("^user clicks on the save survey button$")
	public void user_click_on_the_save_survey_btn() throws Throwable {
			
		surveyPage.clickSaveSurveyButton();
	}
	
	@Then("^verify that message appears stating the answers are saved$")
	public void verify_survey_form_is_saved() throws Throwable {
			
		waitFortheElement(60, surveyPage.getSurveyMessage());
		Assert.assertTrue(surveyPage.getSurveyMessage().getText().contains("Your responses saved successfully"));
	}
	
	@Then("^user clicks on the submit survey button$")
	public void user_click_on_the_submit_survey_btn() throws Throwable {
			
		surveyPage.clickSubmitSurveyButton();
	}
	
	@Then("^verify that message appears stating the answers are submitted$")
	public void verify_survey_form_is_submitted() throws Throwable {
	
		waitFortheElement(60, surveyPage.getSurveyMessage());
		Assert.assertTrue(surveyPage.getSurveyMessage().getText().contains("Your responses submitted successfully."));
	}
	
	@Then("^user clicks on the close survey button$")
	public void user_click_on_the_close_survey_btn() throws Throwable {
			
		surveyPage.clickCloseSurveyButton();
	}
	
}
