package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.PersonalDetailsPage;

public class PersonalDetailsPageSteps extends Base{
	
	
	PersonalDetailsPage personalDetailsPage;
	TestContext testContext;
	
	public PersonalDetailsPageSteps(TestContext context) {
		testContext = context;
		personalDetailsPage = testContext.getPageObjectManager().getPersonalDetailsPage();
	}

	
	@Then("^verify that family information should display on the page$")
	public void verify_that_family_information_should_display_on_the_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(personalDetailsPage.getPersonalDetailText().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(personalDetailsPage.getCardInfoText().getText().contains(object.get(0).get(1)));
		Assert.assertTrue(personalDetailsPage.getBankInfoText().getText().contains(object.get(0).get(2)));
	}
	
	@Then("^verify that dental coverage page should display$")
	public void verify_that_dental_coverage_page_should_display(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(personalDetailsPage.getenrollesText().getText().contains(object.get(0).get(0)));
	}
	
	@When("^user click on the view details link$")
	public void user_click_on_the_view_details_link() throws Throwable {
		personalDetailsPage.clickViewDetailsLink();
	}

	@Then("^verify dependent table on the personal detail page$")
	public void verify_dependent_table_on_the_personal_detail_page(DataTable data) throws Throwable {
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(personalDetailsPage.getDependentFullName().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(personalDetailsPage.getDependentRelationship().getText().contains(object.get(0).get(1)));

	}

	@Then("^user click on the member full name$")
	public void user_click_on_the_member_full_name() throws Throwable {
		personalDetailsPage.clickMemberFullName();
	}
	
	@Then("^verify card information is displayed on the page$")
	public void verify_card_information_is_displayed_on_the_page(DataTable data) throws Throwable {
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(personalDetailsPage.getCardInformation().getText().contains(object.get(0).get(0)));
	}

	@Then("^user click on the coverage link$")
	public void user_click_on_the_coverage_link() throws Throwable {
		personalDetailsPage.clickCoverageLink();
	}

	@Then("^user click on the dental link$")
	public void user_click_on_the_dental_link() throws Throwable {
		personalDetailsPage.clickActiveLink();
	}
}
