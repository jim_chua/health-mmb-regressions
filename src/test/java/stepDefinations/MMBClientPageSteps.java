package stepDefinations;

import cucumber.api.java.en.Then;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.MMBClientPage;

public class MMBClientPageSteps extends Base{
	
	MMBClientPage mmbClientPage;
	TestContext testContext;

	public MMBClientPageSteps(TestContext context) {
		testContext = context;
		mmbClientPage = testContext.getPageObjectManager().getMMBClientPage();
	}
	
	
	

	@Then("^verify that \"([^\"]*)\" is displayed in the dropdown$")
	public void verify_that_is_displayed_in_the_dropdown(String element) throws Throwable {
		staticwait(2);
	    mmbClientPage.getEmployeeListDropdownOptions(element);
	}
	
	
	@Then("^verify that export link is displayed on the page$")
	public void verify_that_export_link_is_displayed_on_the_page() throws Throwable {
		staticwait(1);
	    mmbClientPage.getExportLink().getText().contains("Export All");
	}

}
