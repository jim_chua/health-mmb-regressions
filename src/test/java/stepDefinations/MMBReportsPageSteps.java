package stepDefinations;

import cucumber.api.java.en.Then;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.MMBReportsPage;

public class MMBReportsPageSteps extends Base {
	
	MMBReportsPage mmbReportsPage;
	TestContext testContext;

	public MMBReportsPageSteps(TestContext context) {
		testContext = context;
		mmbReportsPage = testContext.getPageObjectManager().getMMBReportsPage();
	}
	
	
	
	
	@Then("^verify that audit trail for the enrolment link should displayed$")
	public void verify_that_audit_trail_for_the_enrolment_link_should_displayed() throws Throwable {
	   mmbReportsPage.getExportButton();
	}

	@Then("^verify that invoice details page should displayed$")
	public void verify_that_invoice_details_page_should_displayed() throws Throwable {
		mmbReportsPage.getExportButton();
	}

}
