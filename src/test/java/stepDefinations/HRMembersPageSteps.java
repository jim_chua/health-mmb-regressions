package stepDefinations;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.HRMembersPage;

public class HRMembersPageSteps extends Base {
	
	HRMembersPage hrMembersPage;
	TestContext testContext;
	
	public HRMembersPageSteps(TestContext context) {
		testContext = context;
		hrMembersPage = testContext.getPageObjectManager().getHRMembersPage();
	}
	
	
	@When("^user click on the coverage tab$")
	public void user_click_on_the_coverage_tab() throws Throwable {
	   hrMembersPage.clickCoverageTab();
	}

	@Then("^verify that \"([^\"]*)\" text is displayed on the page$")
	public void verify_that_text_is_displayed_on_the_page(String name) throws Throwable {
		waitFortheElement(60, hrMembersPage.getCoveragePageText());
		Assert.assertTrue(hrMembersPage.getCoveragePageText().getText().contains(name));
	}
	
	@When("^user click on the dependent tab$")
	public void user_click_on_the_dependent_tab() throws Throwable {
	    hrMembersPage.clickDependentsTab();
	}
	
	@When("^user click on the dependent member full name$")
	public void user_click_on_the_dependent_member_full_name() throws Throwable {
	    hrMembersPage.clickDependentMemberFullName();
	}

	@Then("^verify that dependent \"([^\"]*)\" id displayed on the page$")
	public void verify_that_dependent_id_displayed_on_the_page(String name) throws Throwable {
		waitFortheElement(60, hrMembersPage.getDependentCardInformation());
		Assert.assertTrue(hrMembersPage.getDependentCardInformation().getText().contains(name));
	}
	

	@Then("^\"([^\"]*)\" page should display$")
	public void page_should_display(String name) throws Throwable {
		waitFortheElement(60, hrMembersPage.getMembersLink());
		Assert.assertTrue(hrMembersPage.getMembersLink().getText().contains(name));
	}

	
	@Then("^verfiy \"([^\"]*)\" link should display on the page$")
	public void verfiy_link_should_display_on_the_page(String name) throws Throwable {
		waitFortheElement(60, hrMembersPage.getExportLink());
		Assert.assertTrue(hrMembersPage.getExportLink().getText().contains(name));
	}
	
	@Then("^user click on the members movement link$")
	public void user_click_on_the_members_movement_link() throws Throwable {
	   hrMembersPage.clickMembersMovementLink();
	}
	
	@When("^user click on the members full name$")
	public void user_click_on_the_members_full_name() throws Throwable {
	    hrMembersPage.clickMembersFullName();
	}

	@Then("^verify tha \"([^\"]*)\" text is diaplyed on the page$")
	public void verify_tha_text_is_diaplyed_on_the_page(String name) throws Throwable {
		waitFortheElement(60, hrMembersPage.getCardInformation());
		Assert.assertTrue(hrMembersPage.getCardInformation().getText().contains(name));
	}

	
	
}
