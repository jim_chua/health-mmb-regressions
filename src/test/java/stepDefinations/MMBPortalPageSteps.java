package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.MMBPortalPage;

public class MMBPortalPageSteps extends Base {

	MMBPortalPage mmbPortalPage;
	TestContext testContext;

	public MMBPortalPageSteps(TestContext context) {
		testContext = context;
		mmbPortalPage = testContext.getPageObjectManager().getMMBPortalPage();
	}

	@Then("^portal setup page should be displayed$")
	public void portal_setup_page_should_be_displayed() throws Throwable {
		mmbPortalPage.portalHomePageText().getText().contains("Portals Setup");
	}

	@Then("^setting dropdown should be displayed$")
	public void setting_dropdown_should_be_displayed() throws Throwable {
		mmbPortalPage.portalSettingViewPageText().getText().contains("Portals Setup (View)");
	}

	@Then("^user clicks on Setting dropdown link$")
	public void user_clicks_on_Setting_dropdown_link() throws Throwable {
		mmbPortalPage.clickSettingLink();
	}

	@Then("^verify admin portal settings name should be displayed in the dropdown list$")
	public void verify_admin_portal_settings_name_should_be_displayed_in_the_dropdown_list(DataTable data)
			throws Throwable {
		List<List<String>> object = data.raw();
		Thread.sleep(2000);
		waitFortheElement(10, mmbPortalPage.helpLinkText());
		Assert.assertTrue(mmbPortalPage.helpLinkText().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(mmbPortalPage.clientLegalLinkText().getText().contains(object.get(0).get(1)));
	}

	@Then("^verify view link should be displayed under password setting policy text$")
	public void verify_view_link_should_be_displayed_under_password_setting_policy_text() throws Throwable {
		mmbPortalPage.PasswordSettingViewLinkText().getText().contains("View");
	}

	@Then("^user selects view link under password setting policy$")
	public void user_selects_view_link_under_password_setting_policy() throws Throwable {
		mmbPortalPage.clickPasswordSettingViewLink();
	}

	@Then("^verify password setting policy view page should be displayed$")
	public void verify_password_setting_policy_view_page_should_be_displayed() throws Throwable {
		mmbPortalPage.passwordSettingViewPageText().getText().contains("Portals Setup (Password Policy)");
	}

	@Then("^user selects view link under portal setting policy$")
	public void user_selects_view_link_under_portal_setting_policy() throws Throwable {
		mmbPortalPage.clickPortalSettingViewLink();
	}

	@Then("^verify portal setting policy view page should be displayed$")
	public void verify_portal_setting_policy_view_page_should_be_displayed() throws Throwable {
		mmbPortalPage.portalSettingViewPageText().getText().contains("Portals Setup (View)");
	}

	@Then("^verify view link should be displayed under portal setting policy text$")
	public void verify_view_link_should_be_displayed_under_portal_setting_policy_text() throws Throwable {
		mmbPortalPage.PortalSettingViewLinkText().getText().contains("View");
	}

}
