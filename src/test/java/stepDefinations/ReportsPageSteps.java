package stepDefinations;

import cucumber.api.java.en.Then;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.ReportsPage;

public class ReportsPageSteps extends Base {

	ReportsPage reportsPage;
	TestContext testContext;

	public ReportsPageSteps(TestContext context) {
		testContext = context;
		reportsPage = testContext.getPageObjectManager().getReportsPage();
	}

	
	/*  @Then("^user click on the export button and reprot should download$") public
	  void user_click_on_the_export_button_and_reprot_should_download() throws
	 Throwable { reportsPage.clickExportButton(); 
	 }*/
	 

	@Then("^export button should be enabled$")
	public void export_button_should_be_enabled() throws Throwable {
		reportsPage.getExportButton();
	}
	
	@Then("^view offline report page should displayed$")
	public void view_offline_report_page_should_displayed() throws Throwable {
		waitFortheElement(30, reportsPage.getViewOfflineReportPage());
	    reportsPage.getViewOfflineReportPage().getText().contains("There is no offline reporting as of current date.");
	}

}
