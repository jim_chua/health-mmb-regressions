package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.EmployeeHomePage;

public class EmployeeHomePageSteps extends Base {

	EmployeeHomePage employeeHomePage;
	TestContext testContext;
	
	public EmployeeHomePageSteps(TestContext context) {
		testContext = context;
		employeeHomePage = testContext.getPageObjectManager().getEmployeeHomePage();
		
	}

	@Then("^verify selected (.+) items are displayed in Check out page$")
	public void verify_selected_Personal_Details_items_are_displayed_in_Check_out_page(String name) throws Throwable {
		waitFortheElement(60, employeeHomePage.getHomePageText());
		Assert.assertTrue(employeeHomePage.getHomePageText().getText().contains(name));
	}

	@Then("^verify selected (.+) item name are displayed in Check out page$")
	public void verify_selected_your_claim_items_are_displayed_in_Check_out_page(String name) throws Throwable {
		waitFortheElement(60, employeeHomePage.getYourClaimText());
		Assert.assertTrue(employeeHomePage.getYourClaimText().getText().contains(name));
	}


	@Then("^verify NRIC fields on the home page$")
	public void verify_NRIC_fields_on_the_home_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		waitFortheElement(60, employeeHomePage.getmaritalStatus());
		Thread.sleep(2000);
		Assert.assertTrue(employeeHomePage.getmaritalStatus().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(employeeHomePage.getGender().getText().contains(object.get(0).get(1)));
		Assert.assertTrue(employeeHomePage.getDOB().getText().contains(object.get(0).get(2)));
		Assert.assertTrue(employeeHomePage.getPhone().getText().contains(object.get(0).get(3)));
		Assert.assertTrue(employeeHomePage.getEmail().getText().contains(object.get(0).get(4)));
		Assert.assertTrue(employeeHomePage.getDependent().getText().contains(object.get(0).get(5)));

	}

	@When("^user select all claims link$")
	public void user_select_all_claims_link() throws Throwable {
		employeeHomePage.clickAllClaimsLink();
	}

	@When("^user click on the home button$")
	public void user_click_on_the_home_button() throws Throwable {
		employeeHomePage.clickHomeLink();
	}

	@When("^user select my family information link$")
	public void user_select_my_family_information_link() throws Throwable {
		employeeHomePage.clickMyFamilyInfoLink();
	}

	@When("^user click on the contact insurer link$")
	public void user_click_on_the_contact_insurer_link() throws Throwable {
		employeeHomePage.clickContactInsurerLink();
	}
	
	
	/*Added by Joann*/
	@When("^user select surveys link$")
	public void user_select_surveys_link() throws Throwable {
		employeeHomePage.clickSurveysLink();
	}
	
	/*Added by Joann*/
	@When("^user clicks on the latest survey link$")
	public void user_click_on_the_latest_surveys_link() throws Throwable {
		employeeHomePage.clickLatestSurveyLink();
	}
	
	/*Added by Joann*/
	@Then("^verify that documents panel is displayed$")
	public void verify_documents_container() throws Throwable {
		waitFortheElement(60, employeeHomePage.getDocumentList());
		waitFortheElement(60, employeeHomePage.getViewAllDocsLink());
	}
	
	/*Added by Joann*/
	@When("^user clicks on view all documents link$")
	public void user_click_on_the_view_docs_link() throws Throwable {
		employeeHomePage.clickViewAllDocsLink();
	}


}
