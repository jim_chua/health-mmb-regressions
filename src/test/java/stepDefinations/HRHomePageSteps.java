package stepDefinations;

import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.HRHomePage;

public class HRHomePageSteps extends Base{
	
	HRHomePage hrHomePage;
	TestContext testContext;
	
	public HRHomePageSteps(TestContext context) {
		testContext = context;
		hrHomePage = testContext.getPageObjectManager().getHomePage();
	}
	
	@Then("^user click on the view offline report link$")
	public void user_click_on_the_view_offline_report_link() throws Throwable {
	    hrHomePage.clickViewOfflineReport();
	}
	
	@Then("^user click on the excess claims report link$")
	public void user_click_on_the_excess_claims_report_link() throws Throwable {
	    hrHomePage.clickExcessClaims();
	}
	
	@Then("^user click on the billing information report link$")
	public void user_click_on_the_billing_information_report_link() throws Throwable {
	    hrHomePage.clickBillingInformation();
	}
	
	@Then("^user click on the historical claims report link$")
	public void user_click_on_the_historical_claims_report_link() throws Throwable {
	    hrHomePage.clickHistoricalClaims();
	}
	
	@Then("^user click on the claim summary report link$")
	public void user_click_on_the_claim_summary_report_link() throws Throwable {
	   hrHomePage.clickClaimSummary();
	}
	
	
	@Then("^user click on the outstanding claims link$")
	public void user_click_on_the_outstanding_claims_link() throws Throwable {
	    hrHomePage.clickOutstandingClaims();
	}
	
	@Then("^user click on the benefit utilisation link$")
	public void user_click_on_the_benefit_utilisation_link() throws Throwable {
	   hrHomePage.clickBenefitUtilization();
	}
	
	
	@Then("^user click on the benefit information link$")
	public void user_click_on_the_benefit_information_link() throws Throwable {
	    hrHomePage.clickBenefitInformation();
	}
	
	@Then("^user click on the member listing link$")
	public void user_click_on_the_member_listing_link() throws Throwable {
	   hrHomePage.clickMemberListing();
	}
	
	
	@Then("^user click on the claims tab$")
	public void user_click_on_the_claims_tab() throws Throwable {
		hrHomePage.clickClaimsTab();
	}

	@When("^user click on the products tab$")
	public void user_click_on_the_products_tab() throws Throwable {
	    hrHomePage.clickProductTab();
	}

	@Then("^user click on the members tab$")
	public void user_click_on_the_members_tab() throws Throwable {
	   hrHomePage.clickMembersTab();
	}
	
	@Then("^verify report links are displayed on the page$")
	public void verify_report_links_are_displayed_on_the_page(DataTable data) throws Throwable {
		List<List<String>> object = data.raw();
		waitFortheElement(60, hrHomePage.getmembersListing());
		Thread.sleep(2000);
		Assert.assertTrue(hrHomePage.getmembersListing().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(hrHomePage.getbenefitInformation().getText().contains(object.get(0).get(1)));
		Assert.assertTrue(hrHomePage.getbenefitUtilization().getText().contains(object.get(0).get(2)));
		Assert.assertTrue(hrHomePage.getoutstandingClaims().getText().contains(object.get(0).get(3)));
		Assert.assertTrue(hrHomePage.getclaimSummary().getText().contains(object.get(0).get(4)));
		Assert.assertTrue(hrHomePage.gethistoricalClaims().getText().contains(object.get(0).get(5)));
		Assert.assertTrue(hrHomePage.getbillingInformation().getText().contains(object.get(0).get(6)));
		Assert.assertTrue(hrHomePage.getexcessClaims().getText().contains(object.get(0).get(7)));
	}


}
