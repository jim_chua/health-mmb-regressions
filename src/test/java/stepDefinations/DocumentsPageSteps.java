package stepDefinations;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumberUtilities.Base;
import cucumberUtilities.TestContext;
import pageObjects.DocumentsPage;

public class DocumentsPageSteps extends Base {

	DocumentsPage documentsPage;
	TestContext testContext;

	public DocumentsPageSteps(TestContext context) {
		testContext = context;
		documentsPage = testContext.getPageObjectManager().getDocumentsPage();
	}

	@Then("^verify that documents page is displayed$")
	public void verify_documents_page() throws Throwable {
		
		waitFortheElement(60, documentsPage.getDocumentsHeaderName());
		waitFortheElement(60, documentsPage.getDocumentsList());
	}
	
	@Then("^user searches for a document with name (.+)")
	public void user_search_document(String document) throws Throwable {
		documentsPage.enterDocSearchText(document);
		documentsPage.clickSearchButton();
	}
	
	@Then("^verify that at least one document with name (.+) is returned$")
	public void verify_search_document_result(String document) throws Throwable {
		
		waitFortheElement(60, documentsPage.getSearchDocResult());
		Assert.assertTrue(documentsPage.getSearchDocResult().getText().contains(document));
	}
	
}
