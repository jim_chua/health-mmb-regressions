package cucumbersManagers;

import org.openqa.selenium.WebDriver;

import pageObjects.AllClaimsPage;
import pageObjects.ContactInsurerPage;
import pageObjects.DocumentsPage;
import pageObjects.EEPortalAdminPage;
import pageObjects.EEPortalAdminQuestionPage;
import pageObjects.EmployeeHomePage;
import pageObjects.HRClaimsPage;
import pageObjects.HRHomePage;
import pageObjects.HRMembersPage;
import pageObjects.HRProductPage;
import pageObjects.LoginPage;
import pageObjects.MMBClientPage;
import pageObjects.MMBHomePage;
import pageObjects.MMBPortalPage;
import pageObjects.MMBReportsPage;
import pageObjects.PersonalDetailsPage;
import pageObjects.ReportsPage;
import pageObjects.SurveyPage;

public class PageObjectManager {

	private WebDriver driver;

	private LoginPage loginPage;

	private HRHomePage hrHomePage;

	private PersonalDetailsPage personalDetailsPage;

	private AllClaimsPage allClaimsPage;

	private ContactInsurerPage contactInsurerPage;

	private EmployeeHomePage employeeHomePage;

	private HRProductPage hrProductPage;

	private HRMembersPage hrMembersPage;

	private HRClaimsPage hrClaimsPage;

	/* Added by Joann */
	private SurveyPage surveyPage;

	/* Added by Joann */
	private DocumentsPage documentsPage;

	private ReportsPage reportsPage;

	private MMBHomePage mmbHomePage;

	private MMBPortalPage mmbPortalPage;
	
	private MMBClientPage mmbClientPage;
	
	private MMBReportsPage mmbReportsPage;
	
	private EEPortalAdminPage eePortalAdminPage;
	
	private EEPortalAdminQuestionPage eePortalAdminQuestionPage;

	// Constructor
	public PageObjectManager(WebDriver driver) {

		this.driver = driver;

	}
	
	public EEPortalAdminQuestionPage getEEPortalAdminQuestionPage() {

		return (eePortalAdminQuestionPage == null) ? eePortalAdminQuestionPage = new EEPortalAdminQuestionPage(driver) : eePortalAdminQuestionPage;

	}
	
	public EEPortalAdminPage getEEPortalAdminPage() {

		return (eePortalAdminPage == null) ? eePortalAdminPage = new EEPortalAdminPage(driver) : eePortalAdminPage;

	}
	
	public MMBReportsPage getMMBReportsPage() {

		return (mmbReportsPage == null) ? mmbReportsPage = new MMBReportsPage(driver) : mmbReportsPage;

	}

	public MMBClientPage getMMBClientPage() {

		return (mmbClientPage == null) ? mmbClientPage = new MMBClientPage(driver) : mmbClientPage;

	}
	
	public MMBPortalPage getMMBPortalPage() {

		return (mmbPortalPage == null) ? mmbPortalPage = new MMBPortalPage(driver) : mmbPortalPage;

	}

	public MMBHomePage getMMBHomePage() {

		return (mmbHomePage == null) ? mmbHomePage = new MMBHomePage(driver) : mmbHomePage;

	}

	public ReportsPage getReportsPage() {

		return (reportsPage == null) ? reportsPage = new ReportsPage(driver) : reportsPage;

	}

	public HRClaimsPage getHRClaimsPage() {

		return (hrClaimsPage == null) ? hrClaimsPage = new HRClaimsPage(driver) : hrClaimsPage;

	}

	public HRHomePage getHomePage() {

		return (hrHomePage == null) ? hrHomePage = new HRHomePage(driver) : hrHomePage;

	}

	public LoginPage getLoginPage() {

		return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;

	}

	public PersonalDetailsPage getPersonalDetailsPage() {

		return (personalDetailsPage == null) ? personalDetailsPage = new PersonalDetailsPage(driver)
				: personalDetailsPage;

	}

	public AllClaimsPage getAllClaimsPage() {

		return (allClaimsPage == null) ? allClaimsPage = new AllClaimsPage(driver) : allClaimsPage;

	}

	public ContactInsurerPage getContactInsurerPage() {

		return (contactInsurerPage == null) ? contactInsurerPage = new ContactInsurerPage(driver) : contactInsurerPage;

	}

	public EmployeeHomePage getEmployeeHomePage() {

		return (employeeHomePage == null) ? employeeHomePage = new EmployeeHomePage(driver) : employeeHomePage;

	}

	public HRProductPage getHRProductPage() {

		return (hrProductPage == null) ? hrProductPage = new HRProductPage(driver) : hrProductPage;

	}

	public HRMembersPage getHRMembersPage() {

		return (hrMembersPage == null) ? hrMembersPage = new HRMembersPage(driver) : hrMembersPage;

	}

	/* Added by Joann */
	public SurveyPage getSurveyPage() {

		return (surveyPage == null) ? surveyPage = new SurveyPage(driver) : surveyPage;

	}

	/* Added by Joann */
	public DocumentsPage getDocumentsPage() {

		return (documentsPage == null) ? documentsPage = new DocumentsPage(driver) : documentsPage;

	}

}
