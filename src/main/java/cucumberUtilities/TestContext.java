package cucumberUtilities;

import cucumbersManagers.PageObjectManager;
import cucumbersManagers.WebDriverManager;

public class TestContext  {

	public WebDriverManager webDriverManager;
	public PageObjectManager pageObjectManager;
	

	public TestContext() {
	//	webDriverManager = new WebDriverManager();
		//pageObjectManager = new PageObjectManager(webDriverManager.getDriver());
	}

	public WebDriverManager getWebDriverManager() {
		return webDriverManager;
	}

	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}
	
	

}
