package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class DocumentsPage extends Base {
	
	WebDriver driver;
	
	@FindBy(xpath = "//h1[contains(text(),'List of Documents')]")
	WebElement documentsHeader;
	
	@FindBy(xpath = "//*[contains(@id,'_DocumentViewer_gvDocuments')]")
	WebElement documentsList;
	
	@FindBy(xpath = "//*[contains(@id,'txtDocName')]")
	WebElement documentsTextfield;
	
	@FindBy(xpath = "//*[contains(@id,'_DocumentViewer_btnSearch')]")
	WebElement searchDocButton;
	
	@FindBy(xpath = "//*[contains(@id,'_DocumentViewer_gvDocuments_lnkDownload_0')]")
	WebElement document1Name;
	
	public DocumentsPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getDocumentsHeaderName() 
	{
		return documentsHeader;
	}
	
	public WebElement getDocumentsList() 
	{
		return documentsList;
	}
	
	public WebElement getSearchDocResult() 
	{
		return document1Name;
	}
	
	public void enterDocSearchText(String search) 
	{
		enterText(documentsTextfield, search);
	}
	
	public void clickSearchButton() 
	{
		clickButton(searchDocButton);
	}
	
}
