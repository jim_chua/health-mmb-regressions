package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class EmployeeHomePage extends Base  {

	 WebDriver driver;
	
	@FindBy(xpath = "//*[contains(@id, '_lblPersonalDetails')]")
	WebElement personalDetailText;
	
	@FindBy(xpath = "//*[contains(@id, '_lblYourClaim')]")
	WebElement yourClaimText;
	
	@FindBy(xpath = "//*[contains(@id, '_lblMaritalStatusHeader')]")
	WebElement maritalStatusText;
	
	@FindBy(xpath = "//*[contains(@id, '_lblGenderHeader')]")
	WebElement genderText;
	
	@FindBy(xpath = "//*[contains(@id, '_lblDOBHeader')]")
	WebElement DOBText;
	
	@FindBy(xpath = "//*[contains(@id, '_lblEmailHeader')]")
	WebElement emailText;
	
	@FindBy(xpath = "//*[contains(@id, '_lblPhoneHeader')]")
	WebElement phoneText;
	
	@FindBy(xpath = "//*[contains(@id, '_lblDependentsHeader')]")
	WebElement dependentText;
	
	@FindBy(xpath = "//*[@id='dvEmployeeMenu']//following::i[1]")
	WebElement homeToggleLink;
	
	@FindBy(xpath = "//a[contains(text(),'My Family Information')]")
	WebElement myFamilyInformationLink;
	
	@FindBy(xpath = "//a[contains(text(),'All Claims')]")
	WebElement allClaimsLink;
	
	@FindBy(xpath = "//*[contains(@id, '_lblInsurerContactLinkDetail')]")
	WebElement contactInsurerLink;
	
	/*Added by Joann*/
	@FindBy(xpath = "//a[contains(text(),'Surveys')]")
	WebElement surveysLink;
	
	/*Added by Joann*/
	@FindBy(xpath = "//*[contains(@id,'_lblLatestSurvey')]")
	WebElement latestSurveyLink;
	
	/*Added by Joann*/
	@FindBy(xpath = "//*[contains(@id,'_dlstDocuments')]")
	WebElement documentsList;
	
	/*Added by Joann*/
	@FindBy(xpath = "//*[@class='mul-box-adv mul-tile-ctn documents-tile-ctn']//a[contains(@id,'_lbtnViewAll')]")
	WebElement viewAllDocsLink;
	
	
	public EmployeeHomePage(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickContactInsurerLink() 
	{
		clickJS(contactInsurerLink);
	}

	
	public WebElement getHomePageText() 
	{
		return personalDetailText;
	}
	
	
	public WebElement getYourClaimText() 
	{
		return yourClaimText;
	}
	
	public WebElement getmaritalStatus() 
	{
		return maritalStatusText;
	}
	
	
	public WebElement getGender() 
	{
		return genderText;
	}
	
	public WebElement getDOB() 
	{
		return DOBText;
	}
	
	public WebElement getPhone() 
	{
		return phoneText;
	}
	
	
	public WebElement getEmail() 
	{
		return emailText;
	}
	
	public WebElement getDependent()
	{
		return dependentText;
	}
	
	
	public void clickHomeLink() 
	{
		clickButton(homeToggleLink);
	}
	
	public void clickMyFamilyInfoLink() 
	{
		clickButton(myFamilyInformationLink);
	}
	
	public void clickAllClaimsLink() 
	{
		clickButton(allClaimsLink);
	}
	
	/*Added by Joann*/
	public WebElement getDocumentList()
	{
		return documentsList;
	}
	
	/*Added by Joann*/
	public WebElement getViewAllDocsLink()
	{
		return viewAllDocsLink;
	}
	
	/*Added by Joann*/
	public void clickSurveysLink() 
	{
		clickButton(surveysLink);
	}
	
	/*Added by Joann*/
	public void clickLatestSurveyLink() 
	{
		clickButton(latestSurveyLink);
	}
	
	/*Added by Joann*/
	public void clickViewAllDocsLink() 
	{
		clickButton(viewAllDocsLink);
	}
	
}
