package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class ReportsPage extends Base {
	
	WebDriver driver;
	
	
	@FindBy(xpath = "//*[contains(@id,'ReportViewer_btnSearch')]")
	WebElement exportButton;
	
	
	@FindBy(xpath = "//*[contains(text(),'There is no offline reporting as of current date.')]")
	WebElement viewOfflineReportPage;
	
	
	
	//Constructor
	public ReportsPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	/*public void clickExportButton() {
		clickButton(exportButton);
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	
	public WebElement getViewOfflineReportPage() {
		return viewOfflineReportPage;
	}
	
	public void getExportButton() {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(exportButton.isEnabled()) {
			System.out.println(exportButton.getText() +" button is enabled take your action");
		}else{
			System.out.println(exportButton.getText() +" button is disabled");
		} 
	}

}
