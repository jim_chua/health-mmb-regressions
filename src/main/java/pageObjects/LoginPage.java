package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class LoginPage extends Base {
	
		 WebDriver driver;
		 
		//Page Factory- OR:
		@FindBy(xpath = "//a[@title='Sign In']")
		@CacheLookup
		public
		WebElement signInButton;
		
		
		@FindBy(xpath = "//input[contains(@id, '_MMBPortal_txtUsername')]")
		@CacheLookup
		WebElement username;
		
		@FindBy(xpath = "//*[contains(@id, '_Login_Login_MMBPortal_txtPassword-clone')]")
		@CacheLookup
		WebElement password;
		
		@FindBy(xpath = "//*[contains(@id, '_Login_Login_MMBPortal_txtPassword')]")
		@CacheLookup
		WebElement password1;
		
		@FindBy(xpath = "//input[contains(@id, '_MMBPortal_cmdLogin')]")
		@CacheLookup
		WebElement loginButton;
		
		
		@FindBy(xpath = "//*[contains(@id, '_Login_ctrlLoginType_btnEmployee')]")
		WebElement loginAsEmployee;
		
		@FindBy(xpath = "//*[contains(@id,'_Login_ctrlLoginType_btnHR')]")
		WebElement loginAsHR;
		
		@FindBy(xpath = "//*[contains(@id,'_evoHeader_profileSideBar')]//following::div")
		WebElement logoutArrow;
		
		@FindBy(id = "dnn_evoHeader_lnkLogOut")
		WebElement logout;
		
		
		
		//Constructor
		public LoginPage(WebDriver driver)
		{
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}
		
		public void clickLogoutArrow() {
			clickJS(logoutArrow);
		}
		
		public void clickLogout() {
			clickButton(logout);
		}
		
		
		public void signIn() 
		{	
			clickButton(signInButton);
			//signInButton.click();
			
		}
		
		public void getUsernamePassword(String Username, String Password) 
		{
			enterText(username, Username);
			clickButton(password);
			enterText(password1, Password);
			//username.sendKeys(Username);
			//password.click();
			//password1.sendKeys(Password);
		}
		
		
		public void login() 
		{
			clickButton(loginButton);
			//loginButton.click();
		}
		
		
		public void loginAsEmployee() 
		{
			clickJS(loginAsEmployee);
			
		}
		
		public void loginAsHR()
		{
			clickJS(loginAsHR);
			
		}

}
