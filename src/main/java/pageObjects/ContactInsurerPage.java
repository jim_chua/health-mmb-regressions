package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class ContactInsurerPage extends Base {
	
	 WebDriver driver;
	
	@FindBy(xpath = "//*[contains(@id, 'searchForm')]//following::span")
	WebElement insurerName;
	
	@FindBy(xpath = "//*[contains(@id, 'searchForm')]//following::span[2]")
	WebElement subjectMatterExpert;
	
	//Constructor
	public ContactInsurerPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getInsurerName() 
	{
		return insurerName;
	}
	
	public WebElement getSubjectMatterExpert() 
	{
		return subjectMatterExpert;
	}
	
}
