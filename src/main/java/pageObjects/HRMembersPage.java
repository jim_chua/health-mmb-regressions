package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class HRMembersPage extends Base{
	
	 WebDriver driver;
	
	@FindBy(xpath = "//*[@class='mul-horizontal-list']//a[contains(text(),'Members')]")
	WebElement membersLink;
	
	@FindBy(xpath = "//*[@class='mul-horizontal-list']//a[contains(text(),'Members Movement')]")
	WebElement membersMovementLink;
	
	
	@FindBy(xpath = "//input[contains(@id,'_SearchGrid_btnSearch')]")
	WebElement searchButton;
	
	@FindBy(xpath = "//a[contains(text(),'Export')]")
	WebElement exportLink;
	
	
	@FindBy(xpath = "//div[@class='ngCellText ng-scope']//*[contains(text(),'TIQRI ZULKARNAEN ZUHRI')]")
	WebElement membersFullName;
	
	
	@FindBy(xpath = "//h4[contains(text(),'Card Information')]")
	WebElement cardInformation;
	
	@FindBy(xpath = "//a[contains(text(),'Dependents')]")
	WebElement dependentsTab;
	
	
	@FindBy(xpath = "//h3[contains(text(),'Dependents')]//following::div[@class='ngTopPanel ng-scope']//following::div[@class='ngCellText ng-scope']/a")
	WebElement dependentMemberFullName;
	
	@FindBy(xpath = "//h3[contains(text(),'Card Information')]")
	WebElement dependentCardInformation;
	
	
	@FindBy(xpath = "//h3[contains(text(),'Coverage')]")
	WebElement coveragePage;
	
	
	@FindBy(xpath = "//a[contains(text(),'Coverage')]")
	WebElement coverageTab;
	
	//Constructor
	public HRMembersPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public WebElement getCoveragePageText() {
		return coveragePage;
	}
	
	public void clickCoverageTab() {
		clickJS(coverageTab);
	}
	
	public WebElement getDependentCardInformation() {
		return dependentCardInformation;
	}
	
	public void clickDependentMemberFullName() {
		clickJS(dependentMemberFullName);
	}
	
	public void clickDependentsTab() {
		clickJS(dependentsTab);
	}
	
	public WebElement getMembersLink() 
	{
		return membersLink;
	}
	
	public void clickSearchButton() 
	{
		clickJS(searchButton);
	}
	
	public WebElement getExportLink()
	{
		return exportLink;
	}
	
	public void clickMembersMovementLink() 
	{
		clickJS(membersMovementLink);
	}
	
	public void clickMembersFullName() {
		clickJS(membersFullName);
	}
	
	public WebElement getCardInformation() {
		return cardInformation;
	}

}
