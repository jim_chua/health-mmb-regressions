package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class MMBReportsPage extends Base {
	
	WebDriver driver;
	
	@FindBy(xpath = "//input[contains(@id,'_ODSReportViewer_btnSearch')]")
	WebElement exportButton;
	
	
	//Constructor
		public MMBReportsPage(WebDriver driver) {
			PageFactory.initElements(driver, this);
		}
		
		
		public void getExportButton() {
			if(exportButton.isEnabled())
				System.out.println("button is enabled: Perform user action ");
			else
				System.out.println("button is diabled:");
		}

}
