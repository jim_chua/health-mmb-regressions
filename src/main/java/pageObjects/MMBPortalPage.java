package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class MMBPortalPage extends Base{
	
	WebDriver driver;
	
	@FindBy(xpath = "//*[contains(@id, '_ClientSetup_UP')]//following::div[1]/h1")
	WebElement portalHomePage;

	
	@FindBy(xpath = "//div[@class='dvSetting AlignRight']//following::li/a/img")
	WebElement settingLink;

	
	@FindBy(xpath = "//div[@class='dvSetting AlignRight']//following::a[2]")
	WebElement helpLink;

	
	@FindBy(xpath = "//div[@class='dvSetting AlignRight']//following::a[3]")
	WebElement clientLegalLink;

	
	@FindBy(xpath = "//*[contains(@id, '_ClientSetup_gvClient_lnkPortalSettingStatus')]")
	WebElement portalSettingViewLink;

	
	@FindBy(xpath = "//*[contains(@id, '_ClientSetup_gvClient_lnkPasswordPolicyStatus')]")
	WebElement passwordSettingViewLink;


    @FindBy(xpath = "//*[contains(text(),'Portals Setup (View)')]")
	WebElement portalSettingViewPage;

	
	@FindBy(xpath = "//*[contains(text(),'Portals Setup (Password Policy)')]")
	WebElement passwordSettingViewPage;
	
	
	
	
	//Constructor
	public MMBPortalPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public WebElement portalHomePageText()
	{
	    return portalHomePage;
	}
	
	public WebElement portalSettingViewPageText()
	{
		return portalSettingViewPage;
	}
	
	public WebElement passwordSettingViewPageText()
	{
		return passwordSettingViewPage;
	}
	
	public WebElement settingLinkText()
	{
		return settingLink;
	}
	
	public void clickSettingLink() 
	{
		clickButton(settingLink);
	}
	
	public WebElement helpLinkText()
	{
		return helpLink;
	}
	
	public WebElement clientLegalLinkText()
	{
		return clientLegalLink;
	}
	
	public void clickPortalSettingViewLink() 
	{
		clickButton(portalSettingViewLink);
	}
	
	public void clickPasswordSettingViewLink() 
	{
		clickJS(passwordSettingViewLink);
	}
	
	public WebElement PortalSettingViewLinkText() 
	{
		return portalSettingViewLink;
	}
	
	public WebElement PasswordSettingViewLinkText() 
	{
		return passwordSettingViewLink;
	}

}
