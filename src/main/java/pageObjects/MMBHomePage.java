package pageObjects;

import java.util.List;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import cucumberUtilities.Base;

public class MMBHomePage extends Base {

	WebDriver driver;

	@FindBy(xpath = "//*[contains(@id, '_subHead1_clientName')]")
	WebElement mmbHomePageText;

	@FindBy(xpath = "//nav[@id='dnn_evoHeader_navMenuSimple']//li[contains(@class,'mul-utility-subnav-ctn')]")
	WebElement adminTab;

	@FindBy(xpath = "//a[contains(text(),'Portals Setup')]")
	WebElement portalLink;
	
	
	@FindBy(xpath = "//a[contains(text(),'Client Users')]")
	WebElement clientUsers;
	

	@FindBy(xpath = "//ul[contains(@id, 'evoHeader_menuSimple_menu')]//a[contains(@class,'search-navbar-btn')]")
	WebElement clientLink;

	@FindBy(xpath = "//*[contains(text(),'Choose Another Client')]")
	WebElement chooseAnotherClient;

	@FindBy(xpath = "//select[@id='dnn_evoHeader_selectClient_listOfClients']")
	WebElement chooseAClient;

	@FindBy(xpath = "//*[contains(@id, 'evoHeader_menuSimple_menu')]//following::option[2]")
	WebElement chooseAClientIndonesia;

	@FindBy(xpath = "//input[@id='dnn_evoHeader_selectClient_btnSelectClient']")
	WebElement chooseAClientSelectButton;

	@FindBy(xpath = "//*[contains(@id, 'subHead1_clientName')]")
	WebElement clientNameHeader;
	
	@FindBy(xpath = "//a[contains(text(),'Other')]")
	WebElement othersDropdown;
	
	
	@FindBy(xpath = "//a[contains(text(),'News')]")
	WebElement newsLink;
	
	@FindBy(xpath = "//a[contains(text(),'Published For HR Only')]")
	WebElement publishedForHROnlyText;
	
	
	@FindBy(xpath = "//a[contains(text(),'Audit Trail For Enrollment')]")
	WebElement auditTrailForEnrolment;
	
	@FindBy(xpath = "//a[contains(text(),'Invoice Details')]")
	WebElement invoiceDetails;
	
	

	// Constructor
	public MMBHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	public void clickauditTrailForEnrolment() {
		clickButton(auditTrailForEnrolment);
	}
	
	public void clickInvoiceDetails() {
		clickButton(invoiceDetails);
	}
	
	public WebElement getPublishedForHROnlyText() {
		return publishedForHROnlyText;
	}
	
	public void clickNewsLink() {
		clickButton(newsLink);
	}
	
	public void clickOthersDropdown() {
		clickButton(othersDropdown);
	}
	
	public void clickClientUsersLink() {
		clickButton(clientUsers);
	}

	public void clickAdminTab() {
		clickButton(adminTab);
	}

	public void clickPortalSetupLink() {
		clickButton(portalLink);
	}

	public WebElement getMMBHomePageHeader() {

		return mmbHomePageText;

	}

	public WebElement getMMBClientLink() {

		return clientLink;

	}

	public void clickMMBCLientLink() {
		clickJS(clientLink);
	}

	public WebElement getChooseAnotherClientText() {

		return chooseAnotherClient;

	}

	public WebElement getMMBChooseAClient() {

		return chooseAClient;

	}

	public void clickMMBChooseAClient() {
		Select Object_NM = new Select(chooseAClient);
		List<WebElement>	alloptions	=Object_NM.getAllSelectedOptions();
		for (WebElement selectedItem : alloptions) {
			selectedItem.getText().contains("BenefitMe Indonesia, PT");
		}
		System.out.println(chooseAClient.getText() +"Selected item text is displayed on the page");
	}

	public WebElement getMMBChooseAClientIndonesia() {

		return chooseAClientIndonesia;

	}

	public void clickMMBChooseAClientIndonesia() {
		executeInputOperation(chooseAClient, "BenefitMe Indonesia, PT");
	}

	public WebElement getMMBChooseAClientSelectButton() {

		return chooseAClientSelectButton;

	}

	public void clickMMBChooseAClientSelectButton() {
		clickButton(chooseAClientSelectButton);
		staticwait(5);
	}

	public WebElement getMMBClientNameHeader() {

		return clientNameHeader;

	}

}
