package pageObjects;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import cucumberUtilities.Base;

public class MMBClientPage extends Base{
	
	@FindBy(xpath = "//*[contains(@id, '_ClientUser_ddCategory')]")
	WebElement viewEmployeeList;
	
	@FindBy(xpath = "//*[contains(text(),'Export All')]")
	WebElement exportAllLink;

	// Constructor
	public MMBClientPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void getEmployeeListDropdownOptions(String element) {
		Select dropDownValues= new Select(viewEmployeeList);
		List<WebElement> allOptions=dropDownValues.getAllSelectedOptions();
		for  (WebElement selecteditem : allOptions) {
			selecteditem.getText().contains(element);
		}
		System.out.println(element + "is displayed in the dropdown");
	}
	
	
	public WebElement getExportLink() {
		return exportAllLink;
	}

}
