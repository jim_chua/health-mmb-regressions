package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class EEPortalAdminQuestionPage extends Base {

	WebDriver driver;

	// EE_11
	@FindBy(xpath = "//a[contains(@id,'_SurveyList_gvSurveyList_EditButton_0') and (text()='Edit')]")
	WebElement serveySetupEditLink;

	@FindBy(xpath = "//*[contains(@id,'_CreateSurvey_gvSurveySubGroup_lnkQuestion_0')]")
	WebElement createQuestionLink;

	@FindBy(xpath = "//*[contains(text(),'Survey Setup (Question)')]")
	WebElement serveySetupQuestionHeaderText;

	@FindBy(xpath = "//*[contains(@id,'_Question_txtQuestion')]")
	WebElement serveySetupQuestionBox;

	@FindBy(xpath = "//*[contains(@id,'_Question_cboOptionType')]//option[contains(text(),'Single Selection')]")
	WebElement serveySetupTypeSingle;

	@FindBy(xpath = "//*[contains(@id,'_Question_cboOptionType')]//option[contains(text(),'Multiple Selection')]")
	WebElement serveySetupTypeMultiple;

	@FindBy(xpath = "//*[contains(@id,'_Question_cboOptionType')]//option[contains(text(),'Free Text Input')]")
	WebElement serveySetupTypeInput;

	@FindBy(xpath = "//*[contains(@id,'_Question_txtViewOrder')]")
	WebElement serveySetupDisplayOrder;

	@FindBy(xpath = "//textarea[contains(@id,'_Question_txtOption')]")
	WebElement serveySetupNewOption;

	@FindBy(xpath = "//*[contains(@id,'_Question_chkCorrect')]")
	WebElement serveySetupCorrectCheckBox;

	@FindBy(xpath = "//*[contains(@id,'_Question_cmdAddOption')]")
	WebElement serveySetupAddOption;

	@FindBy(xpath = "//*[contains(@id,'_Question_cmdQuestionUpdate')]")
	WebElement serveySetupSaveButton;

	@FindBy(xpath = "//span[contains(@id,'_ctl00_lblMessage') and (text()='Question created successfully.')]")
	WebElement serveySetupQuestionCreatedMessage;

	// Constructor
	public EEPortalAdminQuestionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void clickSaveButton() {
		clickButton(serveySetupSaveButton);
	}
	
	public WebElement getserveySetupQuestionCreatedMessage() {
		return serveySetupQuestionCreatedMessage;
	}
	
	public void clickserveySetupAddOption() {
		clickButton(serveySetupAddOption);
	}

	public void clickServeySetupCorrectCheckBox() {
		clickButton(serveySetupCorrectCheckBox);
	}
	
	// EE_11
	public void clickServeySetupEditLink() {

		clickButton(serveySetupEditLink);
	}

	public void clickCreateQuestionLink() {

		clickButton(createQuestionLink);
	}

	public WebElement getServeySetupQuestionHeaderText() {

		return serveySetupQuestionHeaderText;

	}

	public void enterServeySetupQuestionBox(String SName) {
		enterText(serveySetupQuestionBox, SName);
	}

	public void clickServeySetupTypeSingle() {

		clickButton(serveySetupTypeSingle);
	}

	public void clickServeySetupTypeMultiple() {

		clickButton(serveySetupTypeMultiple);
	}

	public void clickServeySetupTypeInput() {

		clickButton(serveySetupTypeInput);
	}

	public void enterServeySetupDisplayOrder(String SName) {
		enterText(serveySetupDisplayOrder, SName);
	}

	public void enterServeySetupNewOption(String SName) {
		enterText(serveySetupNewOption, SName);
	}

}
