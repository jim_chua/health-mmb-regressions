package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class HRClaimsPage extends Base {
	
	WebDriver driver;
	
	@FindBy(xpath = "//h1[contains(text(),'Claims')]")
	WebElement claimsText;
	
	@FindBy(xpath = "//input[contains(@id,'_SearchGrid_btnSearch')]")
	WebElement searchButton;
	
	@FindBy(xpath = "//a[contains(text(),'Export')]")
	WebElement exportLink;
	
	@FindBy(xpath = "//a[contains(text(),'Pending')]")
	WebElement pendingLink;
	
	@FindBy(xpath = "//a[contains(text(),'Paid')]")
	WebElement paidLink;
	
	
	@FindBy(xpath = "//a[contains(text(),'Rejected')]")
	WebElement rejectedLink;
	
	
	@FindBy(xpath = "//a[contains(text(),'140200104392')]")
	WebElement pendingClaimNumber1;
	
	@FindBy(xpath = "//a[contains(text(),'293401002041')]")
	WebElement pendingClaimNumber2;
	
	
	@FindBy(xpath = "//*[@id=\"searchForm\"]//following::select")
	WebElement pendingStatus;
	
	
	//Constructor
	public HRClaimsPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectPendingStatusAsCancelled() {
		executeInputOperation(pendingStatus, "Cancelled");
	}
	
	public void clickRejectedLink() {
		clickJS(rejectedLink);
	}
	
	public void clickPaidLink() {
		clickJS(paidLink);
	}
	
	public WebElement getClaimsText() {
		return claimsText;
	}
	
	public void clickSearchButton() {
		clickJS(searchButton);
	}
	
	public WebElement getExportLink() {
		return exportLink;
	}
	
	public void clickPendingLink() {
		clickJS(pendingLink);
	}
	
	public WebElement getPendingClaimNo1() {
		return pendingClaimNumber1;
	}
	
	public WebElement getPendingClaimNo2() {
		return pendingClaimNumber2;
	}


}
