package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class PersonalDetailsPage extends Base {
	
	 WebDriver driver;
	
	//Page Factory- OR:
	@FindBy(xpath = "//*[contains(@id, '_lblViewDetailsHeader')]")
	WebElement viewDetailLink;
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[2]/div/a")
	WebElement dependentMemberFullName;
			
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[2]/div[2]/div/span")
	WebElement dependentRelationship;
	
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[2]/div/a")
	WebElement memberFullName;
	
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[2]/div[2]/div/h4")
	WebElement cardInformation;
	
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/nav/ul/li[2]/div/a")
	WebElement coverageLink;
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[2]/div[2]/div/a")
	WebElement activeLink;
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/h3")
	WebElement enrollesText;
	
	
	@FindBy(xpath = "//h3[contains(text(),'Personal Details')]")
	WebElement personalDetailsText;
	
	@FindBy(xpath = "//h4[contains(text(),'Card Information')]")
	WebElement cardInformationText;
	
	@FindBy(xpath = "//h4[contains(text(),'Bank Information')]")
	WebElement bankInformationText;
	


	//Constructor
	public PersonalDetailsPage(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void clickViewDetailsLink()
	{
		clickButton(viewDetailLink);
	}
	
	public WebElement getDependentFullName() 
	{
		return dependentMemberFullName;
	}
	
	public WebElement getDependentRelationship() 
	{
		return dependentRelationship;
	}
	
	public void clickMemberFullName() 
	{
		clickButton(memberFullName);
	}
	
	public WebElement getCardInformation() 
	{
		return cardInformation;
	}
	
	public void clickCoverageLink() 
	{
		clickButton(coverageLink);
	}
	
	public void clickActiveLink() 
	{
		clickButton(activeLink);
	}
	
	public WebElement getenrollesText() 
	{
		return enrollesText;
	}
	
	public WebElement getPersonalDetailText() 
	{
		return personalDetailsText;
	}
	
	public WebElement getCardInfoText() 
	{
		return cardInformationText;
	}
	
	public WebElement getBankInfoText()
	{
		return bankInformationText;
	}

}
