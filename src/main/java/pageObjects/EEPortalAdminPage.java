package pageObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class EEPortalAdminPage extends Base {

	WebDriver driver;

	@FindBy(xpath = "//*[@class='mul-menu-toggle' and contains(text(),'Admin')]")
	WebElement adminDropDown;

	@FindBy(xpath = "//*[(text()='Content Management')]")
	WebElement contentMangement;

	@FindBy(xpath = "//a[(text()='Survey Setup')]")
	WebElement surveySetup;

	@FindBy(xpath = "//*[@class='mul-hdr-rule' and  (text()='Survey Setup')]")
	WebElement surveySetupPage;

	@FindBy(xpath = "//input[contains(@id,'_SurveyList_btnCreateSurvey')]")
	WebElement ceateSurvey;

	@FindBy(xpath = "//input[contains(@id,'_CreateSurvey_txtSurveyName')]")
	WebElement createSurveyName;

	@FindBy(xpath = "//input[contains(@id,'_CreateSurvey_btnSave')]")
	WebElement surveySaveButton;

	@FindBy(xpath = "//span[contains(text(),'Survey created successfully.')]")
	WebElement createSurveyMessage;

	@FindBy(xpath = "//input[contains(@id,'_CreateSurvey_btnCreateSubGroup')]")
	WebElement createSubGroup;

	@FindBy(xpath = "//input[contains(@id,'_CreateSurvey_txtDescription')]")
	WebElement descriptionText;

	@FindBy(xpath = "//input[contains(@id,'_CreateSurvey_txtDisplayOrder')]")
	WebElement displayOrderText;

	@FindBy(xpath = "//input[contains(@id,'_CreateSurvey_btnSubGroupSave')]")
	WebElement subGroupSaveButton;

	@FindBy(xpath = "//span[contains(text(),'Survey Subgroup added Sucessfully.')]")
	WebElement subGroupSaveMessage;

	@FindBy(xpath = "//input[contains(@id,'_SurveyList_txtSearchSurveyName')]")
	public WebElement enterSurveyName;
		
	@FindBy(xpath = "//input[contains(@id,'_SurveyList_btnSearch')]")
	WebElement clickSurveySearch;

	@FindBy(xpath = "//*[contains(@id,'ctr7330_SurveyList_gvSurveyList')]/tbody/tr[2]/td[1]/span")
	WebElement verifySurveyName;

	@FindBy(xpath = "//a[contains(@id,'_SurveyList_gvSurveyList_PulishedSurvey') and (text()='Publish')]")
	WebElement clickPublish;

	@FindBy(xpath = "//input[contains(@id,'_PublishSurvey_txtPublishStartDate')]")
	WebElement clickStartDate;

	@FindBy(xpath = "//a[@class='ui-state-default ui-state-highlight ui-state-hover']")
	WebElement enterStartDate;

	@FindBy(xpath = "//input[contains(@id,'_PublishSurvey_txtPublishEndDate')]")
	WebElement clickEndDate;

	@FindBy(xpath = "//a[@class='ui-state-default ui-state-highlight ui-state-hover']")
	WebElement enterEndDate;

	@FindBy(xpath = "//input[contains(@id,'_PublishSurvey_txtSurveyBroadcastingDate’)]")
	WebElement clickBroadcastingDate;

	@FindBy(xpath = "//a[@class='ui-state-default ui-state-highlight ui-state-hover']")
	WebElement enterBroadcastingDate;
	
	@FindBy(xpath = "//input[contains(@id,'_PublishSurvey_btnSave')]")
	WebElement publishSurvey;

	@FindBy(xpath = "//span[contains(text(),'Survey Published Successfully.')]")
	WebElement publishMessage;

	@FindBy(xpath = "//input[contains(@id,'_PublishSurvey_txtPublishStartDate')]")
	WebElement startDate;

	@FindBy(xpath = "//input[contains(@id,'_PublishSurvey_txtPublishEndDate')]")
	WebElement endDate;

	@FindBy(xpath = "//input[contains(@id,'_PublishSurvey_txtSurveyBroadcastingDate')]")
	WebElement broadcastingDate;
	
	@FindBy(xpath = "//table[@class='ui-datepicker-calendar']//td")
	List<WebElement> allDatesAvailable;
	
	
	@FindBy(xpath = "//input[contains(@id,'_CreateSurvey_btnBack')]")
	WebElement backButton;
	
	
	

	// Constructor
	public EEPortalAdminPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	public void clickBackButton() {
		clickButton(backButton);
	}

	public void enterBroadcastingDate() {
		clickButton(broadcastingDate);
		
		staticwait(1);
		
		List<WebElement> allDates = allDatesAvailable;
		
		System.out.println(allDates.size());

		for (WebElement ele : allDates) {

			String date = ele.getText();

			if (date.equalsIgnoreCase("24")) {
				ele.click();
				break;
			}

		}
	}

	public void enterStartDate() {
		clickButton(startDate);
		
		staticwait(1);

		List<WebElement> allDates = allDatesAvailable;
		
		System.out.println(allDates.size());

		for (WebElement ele : allDates) {

			String date = ele.getText();

			if (date.equalsIgnoreCase("24")) {
				ele.click();
				break;
			}

		}

	}

	public void enterEndDate() {
		clickButton(endDate); 
		
		staticwait(1);
		
		List<WebElement> allDates = allDatesAvailable;
		
		System.out.println(allDates.size());

		for (WebElement ele : allDates) {

			String date = ele.getText();

			if (date.equalsIgnoreCase("24")) {
				ele.click();
				break;
			}

		}
	}

	public void clickAdminDropDown() {
		clickButton(adminDropDown);
	}

	public void clickContentMnagement() {

		clickButton(contentMangement);

	}

	public void clickSurveySetupLink() {

		clickButton(surveySetup);
	}

	public WebElement getSurveySetupPageText() {

		return surveySetupPage;

	}

	public void clickCreateSurveyButton() {

		clickButton(ceateSurvey);
	}

	public void enterSurveyName() {
		createSurveyName.sendKeys("Survey_New" + genrateRandomNumber());
	}

	public void clickSaveSurveyButton() {

		clickButton(surveySaveButton);
	}

	public WebElement getcreateSurveyMessage() {

		return createSurveyMessage;

	}

	public void clickCreateSubGroupButton() {

		clickButton(createSubGroup);
	}

	public void enterDescription(String Description) {

		enterText(descriptionText, Description);

	}

	public void enterDisplayOrder(String DisplayOrder) {

		enterText(displayOrderText, DisplayOrder);

	}

	public void clickSubGroupSaveButton() {

		clickButton(subGroupSaveButton);
	}

	public WebElement getsubGroupSaveMessage() {

		return subGroupSaveMessage;

	}

	// New

	public void enterSearchSurveyName(String SName) {
		enterText(enterSurveyName, SName);
	}

	public void clickSearchButton() {

		clickButton(clickSurveySearch);
	}
	
	public void clickPublishButtonSurvey() {
		clickButton(publishSurvey);
	}

	public WebElement getSearchSurveyNameText() {

		return verifySurveyName;

	}

	public void clickPublishLink() {

		clickButton(clickPublish);
	}

	public void clickStartDateTextBox() {

		clickButton(clickStartDate);
	}

	public void getStartDate(String SDate) {
		enterText(enterStartDate, SDate);
	}

	public void clickEndDateTextBox() {

		clickButton(clickEndDate);
	}

	public void getEndDate(String EDate) {
		enterText(enterEndDate, EDate);
	}

	public void clickBroadcastingDateTextBox() {

		clickButton(clickBroadcastingDate);
	}

	public void getBroadcastingDate(String BDate) {
		enterText(enterBroadcastingDate, BDate);
	}

	public void clickPublishButton() {

		clickButton(publishSurvey);
	}

	public WebElement getPublishMessage() {

		return publishMessage;

	}
}