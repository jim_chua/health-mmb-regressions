package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class AllClaimsPage extends Base {
	
	 WebDriver driver;
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[2]/div/span")
	WebElement pendingClaimTable;
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/div[2]/div[2]/span")
	WebElement rejectedClaimTable;
	
	@FindBy(xpath = "//*[@id=\"odsContent\"]/div[2]/fieldset/div/div[3]/div[2]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[2]/div/span")
	WebElement paidClaimTable;
	
	@FindBy(xpath = "//*[@class='col-13 first']//following::dd[5]/a")
	WebElement allLink;
	
	@FindBy(xpath = "//*[@class='col-13 first']//following::dd[6]/a")
	WebElement paidLink;
	
	@FindBy(xpath = "//*[@class='col-13 first']//following::dd[7]/a")
	WebElement rejectedLink;
	
	@FindBy(xpath = "//*[@class='col-13 first']//following::dd[8]/a")
	WebElement pendingLink;
	
	@FindBy(xpath = "//h1[contains(text(),'All Claims')]//following::p")
	WebElement viewYourClaimsText;
	
	@FindBy(xpath = "//h1[contains(text(),'All Claims')]")
	WebElement allClaimsText;
	
	@FindBy(xpath = "//a[contains(text(),'Rejected Claims')]")
	WebElement rejectedClaimLink;
	
	@FindBy(xpath = "//a[contains(text(),'Pending Claims')]")
	WebElement pendingClaimLink;
	
	@FindBy(xpath = "//a[contains(text(),'Download Claim Form')]")
	WebElement downloadClaimLink;
	
	
	@FindBy(xpath = "//h1[contains(text(),'Rejected Claims')]")
	WebElement rejectedClaimLinkText;
	
	@FindBy(xpath = "//h1[contains(text(),'Pending Claims')]")
	WebElement pendingClaimLinkText;
	
	@FindBy(xpath = "//h1[contains(text(),'Download Claim Form')]")
	WebElement downloadClaimLinkText;
	
	
	
	public AllClaimsPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	
	
	public WebElement getRejectedClaimLinkText() 
	{
		return rejectedClaimLinkText;
	}
	
	
	public WebElement getPendingClaimLinkText() 
	{
		return pendingClaimLinkText;
	}
	
	public WebElement getDownloadClaimLinkText() 
	{
		return downloadClaimLinkText;
	}
	
	
	public void clickrejectedClaimLink() 
	{
		clickButton(rejectedClaimLink);
	}
	
	public void clickpendingClaimLink() 
	{
		clickButton(pendingClaimLink);
	}
	
	public void clickdownloadClaimLink() 
	{
		clickButton(downloadClaimLink);
	}
	
	public WebElement getAllLink() 
	{
		return allLink;
	}
	
	
	public void clickAllLink()
	{
		clickJS(allLink);
	}
	
	
	public WebElement getPaidLink() 
	{
		return paidLink;
	}
	
	
	public void clickPaidLink() 
	{
		clickJS(paidLink);
	}
	
	public WebElement getRejectedLink() 
	{
		return rejectedLink;
	}
	
	public void clickRejectedLink() 
	{
		clickJS(rejectedLink);
	}
	
	
	public WebElement getPendingLink() 
	{
		return pendingLink;
	}
	
	public void clickPendingLink() 
	{
		clickJS(pendingLink);
		
	}

	public WebElement getviewYourClaim()
	{
		return viewYourClaimsText;
	}
	
	public WebElement getAllClaim() 
	{
		return allClaimsText;
	}
	
	public WebElement getPaidClaimTable() 
	{
		return paidClaimTable;
	}
	
	public WebElement getRejectedClaimTable()
	{
		return rejectedClaimTable;
	}
	
	public WebElement getPendingClaimTable()
	{
		return pendingClaimTable;
	}
	
}
