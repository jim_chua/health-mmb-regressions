package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class SurveyPage extends Base {
	
	WebDriver driver;
	
	@FindBy(xpath = "//h1[contains(text(),'Surveys')]")
	WebElement surveyHeader;
	
	@FindBy(xpath = "//a[contains(@id,'_EmployeeSurveyList_gvSurveyList_lnkSurveyName_0')]")
	WebElement latestSurveyName;
	
	@FindBy(xpath = "//*[contains(@id, 'txtMessage')]")
	WebElement termsAgreementText;
	
	@FindBy(xpath = "//*[contains(@id, '_SubmitSurvey_btnAgree')]")
	WebElement agreeSurveyButton;
	
	@FindBy(xpath = "//*[contains(@id, '_lblSurveyNameValue')]")
	WebElement surveyFormNameText;
	
	
	
	@FindBy(xpath = "//*[contains(@id,'_SubmitSurvey_dlSurveySubGroup_dlQuestion_0_dlOptions_0_optOptions_0')]/tbody/tr[2]/td/label")
	WebElement surveyQ1option2Radio;
	
	@FindBy(xpath = "//*[contains(@id,'_SubmitSurvey_dlSurveySubGroup_dlQuestion_0_dlOptions_1_chkOptions_0')]/tbody/tr[1]/td/label")
	WebElement surveyQ2option1Checkbox;
	
	@FindBy(xpath = "//*[contains(@id,'_SubmitSurvey_dlSurveySubGroup_dlQuestion_0_dlOptions_1_chkOptions_0')]/tbody/tr[3]/td/label")
	WebElement surveyQ2option3Checkbox;
	
	@FindBy(xpath = "//*[contains(@id, 'textRemarksInput_2')]")
	WebElement surveyQ3Textfield;

	@FindBy(xpath = "//*[contains(@id, '_lblMessage')]")
	WebElement surveyMessageText;
	
	@FindBy(xpath = "//*[contains(@id, '_SubmitSurvey_btnSave')]")
	WebElement saveSurveyButton;
	
	@FindBy(xpath = "//*[contains(@id, '_SubmitSurvey_btnSubmit')]")
	WebElement submitSurveyButton;
	
	@FindBy(xpath = "//*[contains(@id, '_SubmitSurvey_btnClose')]")
	WebElement closeSurveyButton;

	
	public SurveyPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getSurveyHeaderName() 
	{
		return surveyHeader;
	}
	
	public WebElement getLatestSurveyName() 
	{
		return latestSurveyName;
	}
	
	public WebElement getTermsAgreementText() 
	{
		return termsAgreementText;
	}
	
	public WebElement getSurveyFormName() 
	{
		return surveyFormNameText;
	}
	
	public WebElement get_Q1option2()
	{
		return surveyQ1option2Radio;
	}
	
	public WebElement get_Q2option1()
	{
		return surveyQ2option1Checkbox;
	}
	
	public WebElement get_Q2option3()
	{
		return surveyQ2option3Checkbox;
	}
	
	public WebElement getSurveyMessage() 
	{
		return surveyMessageText;
	}
	
	public void clickFirstSurvey() 
	{
		clickButton(latestSurveyName);
	}
	
	public void clickAgreeTermsButton() 
	{
		clickButton(agreeSurveyButton);
	}
	
	public void enterSurveyFreeTextAnswer(String answer) 
	{
		enterText(surveyQ3Textfield, answer);
	}
	
	public void clickSaveSurveyButton() 
	{
		clickButton(saveSurveyButton);
	}

	public void clickSubmitSurveyButton() 
	{
		clickButton(submitSurveyButton);
	}
	
	public void clickCloseSurveyButton() 
	{
		clickButton(closeSurveyButton);
	}
}
