package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class HRHomePage extends Base {
	
	 WebDriver driver;
	
	@FindBy(xpath = "//a[contains(text(),'Products')]")
	WebElement productTab;
	
	@FindBy(xpath = "//a[contains(text(),'Members')]")
	WebElement membersTab;
	
	@FindBy(xpath = "//a[contains(text(),'Claims')]")
	WebElement claimsTab;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a")
	WebElement membersListing;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a[2]")
	WebElement benefitInformation;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a[3]")
	WebElement benefitUtilization;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a[4]")
	WebElement outstandingClaims;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a[5]")
	WebElement claimSummary;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a[6]")
	WebElement historicalClaims;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a[7]")
	WebElement billingInformation;
	
	@FindBy(xpath = "//*[contains(@id,'dnn_divMain')]//following::a[8]")
	WebElement excessClaims;
	
	
	@FindBy(xpath = "//*[contains(text(),'View Offline Reports')]")
	WebElement viewOfflineReport;
	
	
	
	//Constructor
	public HRHomePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getexcessClaims() {
		return excessClaims;
	}
	
	public void clickViewOfflineReport() {
		clickButton(viewOfflineReport);
	}
	
	public void clickExcessClaims() {
		clickButton(excessClaims);
	}
	
	public WebElement getbillingInformation() {
		return billingInformation;
	}
	
	public void clickBillingInformation() {
		clickButton(billingInformation);
	}
	
	public WebElement gethistoricalClaims() {
		return historicalClaims;
	}
	
	public void clickHistoricalClaims() {
		clickButton(historicalClaims);
	}
	
	public WebElement getclaimSummary() {
		return claimSummary;
	}
	
	public void clickClaimSummary() {
		clickButton(claimSummary);
	}
	
	public WebElement getoutstandingClaims() {
		return outstandingClaims;
	}
	
	public void clickOutstandingClaims() {
		clickButton(outstandingClaims);
	}
	
	public WebElement getbenefitUtilization() {
		return benefitUtilization;
	}
	
	public void clickBenefitUtilization() {
		clickButton(benefitUtilization);
	}
	
	public WebElement getbenefitInformation() {
		return benefitInformation;
	}
	
	public void clickBenefitInformation() {
		clickButton(benefitInformation);
	}
	
	public WebElement getmembersListing() {
		return membersListing;
	}
	
	public void clickMemberListing() {
		clickButton(membersListing);
	}
	
	public void clickProductTab() 
	{
		clickJS(productTab);
	}
	
	public void clickMembersTab() 
	{
		clickJS(membersTab);
	}
	
	public void clickClaimsTab() {
		clickJS(claimsTab);
	}

}
