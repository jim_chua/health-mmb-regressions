package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;

public class HRProductPage extends Base{
	
	 WebDriver driver;
	
	@FindBy(xpath = "//input[contains(@id,'_SearchGrid_btnSearch')]")
	WebElement searchButton;
	
	
	@FindBy(xpath = "//a[contains(text(),'Active')]")
	WebElement activeLink;
	
	@FindBy(xpath = "//a[contains(text(),'Expired')]")
	WebElement expiredLink;
	
	
	@FindBy(xpath = "//a[contains(text(),'Export')]")
	WebElement exportLink;
	
	
	//Constructor
	public HRProductPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void clickSearchButton() 
	{
		clickJS(searchButton);
	}
	
	
	public WebElement getExportLink() 
	{
		return exportLink;
	}
	
	public WebElement getActiveLink() 
	{
		return activeLink;
	}
	
	public WebElement getExpiredLink() 
	{
		return expiredLink;
	}

	public void clickExpiredLink() 
	{
		clickJS(expiredLink);
	}
}
