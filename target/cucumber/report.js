$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("MMB_01.feature");
formatter.feature({
  "line": 1,
  "name": "Verify features of MMB Dropdown list",
  "description": "",
  "id": "verify-features-of-mmb-dropdown-list",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3738293347,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify dropdown list for select client on MMB Portal",
  "description": "",
  "id": "verify-features-of-mmb-dropdown-list;verify-dropdown-list-for-select-client-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "select client tab should be displayed",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user selects select client tab",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Verify that search navigation bar link is displayed",
  "rows": [
    {
      "cells": [
        "Choose Another Client"
      ],
      "line": 11
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "user clicks on choose a client dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 148738448,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 10507391899,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 4104834,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.select_client_tab_should_be_displayed()"
});
formatter.result({
  "duration": 43425,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_selects_select_client_tab()"
});
formatter.result({
  "duration": 219303063,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.verify_that_search_navigation_bar_link_is_displayed(DataTable)"
});
formatter.result({
  "duration": 2668214697,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_clicks_on_choose_a_client_dropdown()"
});
formatter.result({
  "duration": 246465710,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3731539992,
  "status": "passed"
});
formatter.after({
  "duration": 83692,
  "status": "passed"
});
formatter.after({
  "duration": 1261954569,
  "status": "passed"
});
formatter.uri("MMB_02.feature");
formatter.feature({
  "line": 1,
  "name": "Verify features of MMB Dropdown selection  from list",
  "description": "",
  "id": "verify-features-of-mmb-dropdown-selection--from-list",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3123489259,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify user is able to select client from dropdown list for select client on MMB Portal",
  "description": "",
  "id": "verify-features-of-mmb-dropdown-selection--from-list;verify-user-is-able-to-select-client-from-dropdown-list-for-select-client-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user selects select client tab",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user has selected BenefitMe Indonesia PT from the dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user clicks on select button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "verify BenefitMe Indonesia PT page should display",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 2509562,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 9981159136,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 3109220,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_selects_select_client_tab()"
});
formatter.result({
  "duration": 120978907,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_has_selected_BenefitMe_Indonesia_PT_from_the_dropdown()"
});
formatter.result({
  "duration": 327349651,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_clicks_on_select_button()"
});
formatter.result({
  "duration": 7261898906,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.verify_BenefitMe_Indonesia_PT_page_should_display()"
});
formatter.result({
  "duration": 78362535,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3620937653,
  "status": "passed"
});
formatter.after({
  "duration": 46583,
  "status": "passed"
});
formatter.after({
  "duration": 1137407717,
  "status": "passed"
});
formatter.uri("MMB_03.feature");
formatter.feature({
  "line": 1,
  "name": "Verify features of MMB Portal Setting page",
  "description": "",
  "id": "verify-features-of-mmb-portal-setting-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3187040758,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify various fields and link present on Portal Setting page on MMB Portal",
  "description": "",
  "id": "verify-features-of-mmb-portal-setting-page;verify-various-fields-and-link-present-on-portal-setting-page-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user selects admin dropdown link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user select portal setup from dropdown list",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "portal setup page should be displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "setting dropdown should be displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "user clicks on Setting dropdown link",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "verify admin portal settings name should be displayed in the dropdown list",
  "rows": [
    {
      "cells": [
        "Help Section",
        "Client Legal Info"
      ],
      "line": 14
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 2964734,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 9957992760,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 2691158,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_selects_admin_dropdown_link()"
});
formatter.result({
  "duration": 2247961893,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_select_portal_setup_from_dropdown_list()"
});
formatter.result({
  "duration": 2704902786,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.portal_setup_page_should_be_displayed()"
});
formatter.result({
  "duration": 51699274,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.setting_dropdown_should_be_displayed()"
});
formatter.result({
  "duration": 120055726338,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//*[contains(text(),\u0027Portals Setup (View)\u0027)]\"}\n  (Session info: chrome\u003d77.0.3865.120)\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.4.0\u0027, revision: \u0027unknown\u0027, time: \u0027unknown\u0027\nSystem info: host: \u0027AUME13V0119\u0027, ip: \u002710.52.72.22\u0027, os.name: \u0027Windows Server 2016\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u002712.0.2\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d77.0.3865.40 (f484704e052e0b556f8030b65b953dce96503217-refs/branch-heads/3865@{#442}), userDataDir\u003dC:\\Users\\U81099~1\\AppData\\Local\\Temp\\5\\scoped_dir7704_1729702589}, timeouts\u003d{implicit\u003d0.0, pageLoad\u003d300000.0, script\u003d30000.0}, pageLoadStrategy\u003dnormal, unhandledPromptBehavior\u003ddismiss and notify, strictFileInteractability\u003dfalse, platform\u003dANY, proxy\u003dProxy(), goog:chromeOptions\u003d{debuggerAddress\u003dlocalhost:49167}, acceptInsecureCerts\u003dfalse, browserVersion\u003d77.0.3865.120, browserName\u003dchrome, javascriptEnabled\u003dtrue, platformName\u003dwindows nt, setWindowRect\u003dtrue}]\nSession ID: 6bb9a9a4e9642b5a1399284a142cffda\n*** Element info: {Using\u003dxpath, value\u003d//*[contains(text(),\u0027Portals Setup (View)\u0027)]}\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.base/java.lang.reflect.Constructor.newInstanceWithCaller(Constructor.java:500)\r\n\tat java.base/java.lang.reflect.Constructor.newInstance(Constructor.java:481)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:150)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:115)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:45)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:637)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:410)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:509)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:402)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy24.getText(Unknown Source)\r\n\tat stepDefinations.MMBPortalPageSteps.setting_dropdown_should_be_displayed(MMBPortalPageSteps.java:30)\r\n\tat ✽.Then setting dropdown should be displayed(MMB_03.feature:11)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "MMBPortalPageSteps.user_clicks_on_Setting_dropdown_link()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "MMBPortalPageSteps.verify_admin_portal_settings_name_should_be_displayed_in_the_dropdown_list(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 430466727,
  "status": "passed"
});
formatter.after({
  "duration": 1142161161,
  "status": "passed"
});
formatter.uri("MMB_04.feature");
formatter.feature({
  "line": 1,
  "name": "Verify features of MMB Client Password view setting page",
  "description": "",
  "id": "verify-features-of-mmb-client-password-view-setting-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3225362410,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify password Policy view Setting page on MMB Portal",
  "description": "",
  "id": "verify-features-of-mmb-client-password-view-setting-page;verify-password-policy-view-setting-page-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user selects admin dropdown link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user select portal setup from dropdown list",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "portal setup page should be displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "verify view link should be displayed under password setting policy text",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "user selects view link under password setting policy",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "verify password setting policy view page should be displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 2984473,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 10229361051,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 2404553,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_selects_admin_dropdown_link()"
});
formatter.result({
  "duration": 2295334077,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_select_portal_setup_from_dropdown_list()"
});
formatter.result({
  "duration": 2618450988,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.portal_setup_page_should_be_displayed()"
});
formatter.result({
  "duration": 53595362,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.verify_view_link_should_be_displayed_under_password_setting_policy_text()"
});
formatter.result({
  "duration": 61273272,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.user_selects_view_link_under_password_setting_policy()"
});
formatter.result({
  "duration": 134626552,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.verify_password_setting_policy_view_page_should_be_displayed()"
});
formatter.result({
  "duration": 691489634,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3627785360,
  "status": "passed"
});
formatter.after({
  "duration": 50530,
  "status": "passed"
});
formatter.after({
  "duration": 1152680242,
  "status": "passed"
});
formatter.uri("MMB_05.feature");
formatter.feature({
  "line": 1,
  "name": "Verify features of MMB Client Password view setting page",
  "description": "",
  "id": "verify-features-of-mmb-client-password-view-setting-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3495670344,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify password Policy of view Setting page on MMB Portal",
  "description": "",
  "id": "verify-features-of-mmb-client-password-view-setting-page;verify-password-policy-of-view-setting-page-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user selects admin dropdown link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user select portal setup from dropdown list",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "portal setup page should be displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "verify view link should be displayed under portal setting policy text",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "user selects view link under portal setting policy",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "verify portal setting policy view page should be displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 2740898,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 10069203047,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 2493771,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_selects_admin_dropdown_link()"
});
formatter.result({
  "duration": 2267603749,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_select_portal_setup_from_dropdown_list()"
});
formatter.result({
  "duration": 2598428573,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.portal_setup_page_should_be_displayed()"
});
formatter.result({
  "duration": 49055884,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.verify_view_link_should_be_displayed_under_portal_setting_policy_text()"
});
formatter.result({
  "duration": 50168350,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.user_selects_view_link_under_portal_setting_policy()"
});
formatter.result({
  "duration": 2210108440,
  "status": "passed"
});
formatter.match({
  "location": "MMBPortalPageSteps.verify_portal_setting_policy_view_page_should_be_displayed()"
});
formatter.result({
  "duration": 1202403685,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3915657071,
  "status": "passed"
});
formatter.after({
  "duration": 35924,
  "status": "passed"
});
formatter.after({
  "duration": 1111282135,
  "status": "passed"
});
formatter.uri("MMB_06.feature");
formatter.feature({
  "line": 1,
  "name": "Verify features of MMB Client Password view setting page",
  "description": "",
  "id": "verify-features-of-mmb-client-password-view-setting-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3143371530,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify client set up of admin page on MMB Portal",
  "description": "",
  "id": "verify-features-of-mmb-client-password-view-setting-page;verify-client-set-up-of-admin-page-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user selects admin dropdown link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user select client setup from dropdown list",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify that \"Client HR\" is displayed in the dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "verify that \"Client Employee\" is displayed in the dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "verify that \"Pending Employees\" is displayed in the dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "verify that export link is displayed on the page",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 2648917,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 9992573164,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 3835995,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_selects_admin_dropdown_link()"
});
formatter.result({
  "duration": 2375774295,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_select_client_setup_from_dropdown_list()"
});
formatter.result({
  "duration": 7755673163,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Client HR",
      "offset": 13
    }
  ],
  "location": "MMBClientPageSteps.verify_that_is_displayed_in_the_dropdown(String)"
});
formatter.result({
  "duration": 2261378597,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Client Employee",
      "offset": 13
    }
  ],
  "location": "MMBClientPageSteps.verify_that_is_displayed_in_the_dropdown(String)"
});
formatter.result({
  "duration": 2197214808,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Pending Employees",
      "offset": 13
    }
  ],
  "location": "MMBClientPageSteps.verify_that_is_displayed_in_the_dropdown(String)"
});
formatter.result({
  "duration": 2222566639,
  "status": "passed"
});
formatter.match({
  "location": "MMBClientPageSteps.verify_that_export_link_is_displayed_on_the_page()"
});
formatter.result({
  "duration": 1052957016,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3814604256,
  "status": "passed"
});
formatter.after({
  "duration": 34345,
  "status": "passed"
});
formatter.after({
  "duration": 1145595673,
  "status": "passed"
});
formatter.uri("MMB_07.feature");
formatter.feature({
  "line": 1,
  "name": "Verify news page on MMB Portal",
  "description": "",
  "id": "verify-news-page-on-mmb-portal",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3054400300,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify news page on MMB Portal",
  "description": "",
  "id": "verify-news-page-on-mmb-portal;verify-news-page-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user click on the others dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user click on the news link",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "verify that published for hr lony text should be displyed on the page",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 2452321,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 10138535974,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 2554171,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_click_on_the_others_dropdown()"
});
formatter.result({
  "duration": 2280917417,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_click_on_the_news_link()"
});
formatter.result({
  "duration": 2844842173,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.verify_that_published_for_hr_lony_text_should_be_displyed_on_the_page()"
});
formatter.result({
  "duration": 96782573,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3717691408,
  "status": "passed"
});
formatter.after({
  "duration": 30002,
  "status": "passed"
});
formatter.after({
  "duration": 1145432633,
  "status": "passed"
});
formatter.uri("MMB_08.feature");
formatter.feature({
  "line": 1,
  "name": "Verify news page on MMB Portal",
  "description": "",
  "id": "verify-news-page-on-mmb-portal",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3231991018,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify news page on MMB Portal",
  "description": "",
  "id": "verify-news-page-on-mmb-portal;verify-news-page-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user click on the audit trail for the enrolment link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify that audit trail for the enrolment link should displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 3456224,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 10252094756,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 4172734,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_click_on_the_audit_trail_for_the_enrolment_link()"
});
formatter.result({
  "duration": 4796529248,
  "status": "passed"
});
formatter.match({
  "location": "MMBReportsPageSteps.verify_that_audit_trail_for_the_enrolment_link_should_displayed()"
});
formatter.result({
  "duration": 59616416,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3913261598,
  "status": "passed"
});
formatter.after({
  "duration": 31976,
  "status": "passed"
});
formatter.after({
  "duration": 1126701909,
  "status": "passed"
});
formatter.uri("MMB_09.feature");
formatter.feature({
  "line": 1,
  "name": "Verify reports page on MMB Portal",
  "description": "",
  "id": "verify-reports-page-on-mmb-portal",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3335602744,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify reports page on MMB Portal",
  "description": "",
  "id": "verify-reports-page-on-mmb-portal;verify-reports-page-on-mmb-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MMB"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username \"arjun-bhatia\" and password \"Mercer@12345678\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the mmb home page",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user click on the invoice details link",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "verify that invoice details page should displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user logout from the application",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 2187824,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "arjun-bhatia",
      "offset": 21
    },
    {
      "val": "Mercer@12345678",
      "offset": 49
    }
  ],
  "location": "LoginPageSteps.user_enter_username_and_password(String,String)"
});
formatter.result({
  "duration": 10464764473,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_is_on_the_mmb_home_page()"
});
formatter.result({
  "duration": 2468506,
  "status": "passed"
});
formatter.match({
  "location": "MMBHomePageSteps.user_click_on_the_invoice_details_link()"
});
formatter.result({
  "duration": 4630133065,
  "status": "passed"
});
formatter.match({
  "location": "MMBReportsPageSteps.verify_that_invoice_details_page_should_displayed()"
});
formatter.result({
  "duration": 63703091,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageSteps.user_logout_from_the_application()"
});
formatter.result({
  "duration": 3677035077,
  "status": "passed"
});
formatter.after({
  "duration": 50925,
  "status": "passed"
});
formatter.after({
  "duration": 1137239150,
  "status": "passed"
});
});