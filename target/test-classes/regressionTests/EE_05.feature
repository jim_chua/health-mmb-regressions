Feature: Verify login features of Employee Portal Home Page

@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user select my family information link
Then verify that family information should display on the page
|Personal Details|Card Information|Bank Information|
And user logout from the application


