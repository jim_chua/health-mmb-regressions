Feature: Verify Claims features of HR Portal Home Page

@HR
Scenario: Verify Claims functionality of HR Portal
Given user launched the browser
When user enter username "0180105" and password "Pass123$"
Then user click on the login as HR button
And user click on the claims tab
Then "Claims" text should should display
And user click on the search button
And user click on the rejected link
Then verify that rejected claim detail page should display
And user logout from the application







