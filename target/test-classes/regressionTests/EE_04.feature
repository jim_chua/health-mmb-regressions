Feature: Verify view details page features of Employee Portal Home Page

@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the view details link
Then verify dependent table on the personal detail page
|P ZATA ZAHRA	|Spouse	|
And user logout from the application
	
	
@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the view details link
Then user click on the member full name
Then verify card information is displayed on the page	
|Card Information	|
And user logout from the application


@EE
Scenario: Verify the login functionality of EE Portal
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the view details link
Then user click on the member full name
And user click on the coverage link
And user click on the dental link
Then verify that dental coverage page should display
|Enrollees|
And user logout from the application