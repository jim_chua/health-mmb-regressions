Feature: User should be able to view the list of surveys

@EE
Scenario: Verify the Survey list page
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user select surveys link
Then verify that survey page is displayed
And user logout from the application