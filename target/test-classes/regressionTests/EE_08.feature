Feature: Verify features of Contact Insurer Page

@EE
Scenario: Verify that Rejected claim page should open
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user click on the rejected claim link
Then verify that rejected claim page should display
|Rejected Clams|
And user logout from the application


@EE
Scenario: Verify that Pending claim page should open
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user click on the pending claim link
Then verify that pending claim page should display
|Pending Claims|
And user logout from the application


@EE
Scenario: Verify that Download claim page should open
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user click on the home button
And user click on the download claim link
Then verify that download claim page should display
|Download Claim Form|
And user logout from the application

