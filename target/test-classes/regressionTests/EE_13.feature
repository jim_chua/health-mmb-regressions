Feature: User should be able to take the survey in employee portal

@EE
Scenario: Verify that user has to agree to terms and agreement prior to taking the survey
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
When user clicks on the latest survey link
And user clicks on the first survey in the list
Then verify that terms and agreement page is displayed
Then user clicks on the agree button
Then verify that survey form is displayed
And user logout from the application

@EE
Scenario: Verify that user can opt to save the survey without submitting it yet
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
Then user clicks on the latest survey link
And user clicks on the first survey in the list
Then verify that survey form is displayed
Then user selects options on the survey
|Q1 - option2 |Q2 - option1 |Q2 - option3 |
Then user enters "Q3 answer" on the input field
Then user clicks on the save survey button
Then verify that message appears stating the answers are saved 
And user logout from the application

@EE
Scenario: Verify that user can submit the survey
Given user launched the browser
Then user enter username "0180105" and password "Pass123$"
And user click on the login as employee button
Then user clicks on the latest survey link
And user clicks on the first survey in the list
Then verify that survey form is displayed
Then user clicks on the submit survey button
Then verify that message appears stating the answers are submitted
Then user clicks on the close survey button 
Then verify that survey page is displayed
And user logout from the application