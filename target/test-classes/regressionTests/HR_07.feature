Feature: Verify login features of HR Portal Home Page

@HR
Scenario: Verify the login functionality of HR Portal
Given user launched the browser
When user enter username "0180105" and password "Pass123$"
Then user click on the login as HR button
And user click on the members tab
Then "Members" page should display
And user click on the search button
Then verfiy "Export" link should display on the page
When user click on the members full name
And user click on the dependent tab
And user click on the dependent member full name
Then verify that dependent "Card Information" id displayed on the page
And user logout from the application





